#include "TFT.h"
#include "math.h"

CurrentScreen MainScrTouchDetect(CurrentScreen current,
		CircleArea DetailScreen[][3], MainScrArea SecondScreen[]) {
	TSPoint p, map_p;
	CurrentScreen screen;
	screen = current;

	p = getPoint();
	if (p.z > MINPRES) {
		map_p = getScreenPoint(p);

		if (map_p.x > P1x && map_p.x < P1x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P1y && map_p.y < P1y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen, "PM1.0 pyl zawieszony",
					PM1_0, 2);
		if (map_p.x > P2x && map_p.x < P2x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P2y && map_p.y < P2y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen, "PM2.5 pyl zawieszony",
					PM2_5, 3);
		if (map_p.x > P3x && map_p.x < P3x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P3y && map_p.y < P3y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen, "PM10 pyl zawieszony",
					PM10, 3);
		if (map_p.x > P4x && map_p.x < P4x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P4y && map_p.y < P4y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen, "Wilgotnosc", HUM, 2);
		if (map_p.x > P5x && map_p.x < P5x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P5y && map_p.y < P5y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen, "Dwutlenek wegla", CO2,
					3);
		if (map_p.x > P6x && map_p.x < P6x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P6y && map_p.y < P6y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen, "Formaldehyd", HCHO, 3);
		if (map_p.x > P7x && map_p.x < P7x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P7y && map_p.y < P7y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen, "Temperatura", TEMP, 2);
		if (map_p.x > P8x && map_p.x < P8x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P8y && map_p.y < P8y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen, "Lotne zwiazki org.",
					VOC, 2);
		if (map_p.x > P9x && map_p.x < P9x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P9y && map_p.y < P9y + MAIN_SCR_AREA_HIGTH)
			screen = DisplaySecondScreen(SecondScreen);

		return screen;
	} else
		return screen;
}

CurrentScreen SecondScrTouchDetect(CurrentScreen current,
		CircleArea DetailScreen[][3], MainScrArea MainScreen[]) {
	TSPoint p, map_p;
	CurrentScreen screen;
	screen = current;
	p = getPoint();
	if (p.z > MINPRES) {
		map_p = getScreenPoint(p);

		if (map_p.x > P1x && map_p.x < P1x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P1y && map_p.y < P1y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen,
					"Czast. o rozmiarze < 0.3um w 0.1L", PART_03, 2);
		if (map_p.x > P2x && map_p.x < P2x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P2y && map_p.y < P2y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen,
					"Czast. o rozmiarze < 0.5um w 0.1L", PART_05, 2);
		if (map_p.x > P3x && map_p.x < P3x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P3y && map_p.y < P3y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen,
					"Czast. o rozmiarze < 1um w 0.1L", PART_1, 2);
		if (map_p.x > P4x && map_p.x < P4x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P4y && map_p.y < P4y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen,
					"Czast. o rozmiarze < 2.5um w 0.1L", PART_2_5, 2);
		if (map_p.x > P5x && map_p.x < P5x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P5y && map_p.y < P5y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen,
					"Czast. o rozmiarze < 5um w 0.1L", PART_5, 2);
		if (map_p.x > P6x && map_p.x < P6x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P6y && map_p.y < P6y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen,
					"Czast. o rozmiarze < 10um w 0.1L", PART_10, 2);
		if (map_p.x > P7x && map_p.x < P7x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P7y && map_p.y < P7y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayDetailScreen(DetailScreen, "LPG", LPG, 2);
		if (map_p.x > P8x && map_p.x < P8x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P8y && map_p.y < P8y + MAIN_SCR_AREA_HIGTH)
			screen = DisplaySettingsScreen(&time_to_set,&date_to_set); //Ustawienia do zrobienia!
		if (map_p.x > P9x && map_p.x < P9x + MAIN_SCR_AREA_WIDTH
				&& map_p.y > P9y && map_p.y < P9y + MAIN_SCR_AREA_HIGTH)
			screen = DisplayMainScreen(MainScreen);


		return screen;
	}
	else
		return screen;
}
CurrentScreen DetailScrTouchDetect(CurrentScreen current, Chart* chart,
		MainScrArea MainScreen[], int last24val[][24], int current_h) {
	TSPoint p, map_p;
	CurrentScreen screen;
	screen = current;
	p = getPoint();

	if (p.z > MINPRES) {
		map_p = getScreenPoint(p);
		switch (current) {
		case PM1_0DetailScr:
			if (sqrt(
					(map_p.x - Pcirc3_x2) * (map_p.x - Pcirc3_x2)
							+ (map_p.y - Pcirc3_y2) * (map_p.y - Pcirc3_y2))
					< 30) {
				InitChart(chart, "ug/m3", "godz.", "PM1.0- hist. pomiarow",
						last24val, PM1_0, current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplayMainScreen(MainScreen);
			break;

		case PM2_5DetailScr:
			if (sqrt(
					(map_p.x - Pcirc3_x2) * (map_p.x - Pcirc3_x2)
							+ (map_p.y - Pcirc3_y2) * (map_p.y - Pcirc3_y2))
					< 30) {
				InitChart(chart, "ug/m3", "godz.", "PM2.5- hist. pomiarow",
						last24val, PM2_5, current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplayMainScreen(MainScreen);
			break;
		case PM10DetailScr:
			if (sqrt(
					(map_p.x - Pcirc3_x2) * (map_p.x - Pcirc3_x2)
							+ (map_p.y - Pcirc3_y2) * (map_p.y - Pcirc3_y2))
					< 30) {
				InitChart(chart, "ug/m3", "godz.", "PM10- hist. pomiarow",
						last24val, PM10, current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplayMainScreen(MainScreen);
			break;
		case TempDetailScr:
			if (sqrt(
					(map_p.x - Pcirc2_x2) * (map_p.x - Pcirc2_x2)
							+ (map_p.y - Pcirc2_y2) * (map_p.y - Pcirc2_y2))
					< 30) {
				InitChart(chart, "st. C", "godz.",
						"Temperatura- hist. pomiarow", last24val, TEMP,
						current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplayMainScreen(MainScreen);
			break;
		case HumDetailScr:
			if (sqrt(
					(map_p.x - Pcirc2_x2) * (map_p.x - Pcirc2_x2)
							+ (map_p.y - Pcirc2_y2) * (map_p.y - Pcirc2_y2))
					< 30) {
				InitChart(chart, "%", "godz.", "Wilgtnosc- hist. pomiarow.",
						last24val, HUM, current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplayMainScreen(MainScreen);
			break;
		case CO2DetailScr:
			if (sqrt(
					(map_p.x - Pcirc3_x2) * (map_p.x - Pcirc3_x2)
							+ (map_p.y - Pcirc3_y2) * (map_p.y - Pcirc3_y2))
					< 30) {
				InitChart(chart, "TBD", "godz.",
						"Dwutlenek wegla- hist. pomiarow", last24val, CO2,
						current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplayMainScreen(MainScreen);
			break;
		case HCHODetailScr:
			if (sqrt(
					(map_p.x - Pcirc3_x2) * (map_p.x - Pcirc3_x2)
							+ (map_p.y - Pcirc3_y2) * (map_p.y - Pcirc3_y2))
					< 30) {
				InitChart(chart, "mg/m3", "godz.",
						"Formaldehyd- hist. pomiarow", last24val, HCHO,
						current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplayMainScreen(MainScreen);
			break;
		case VOCDetailScr:
			if (sqrt(
					(map_p.x - Pcirc3_x2) * (map_p.x - Pcirc3_x2)
							+ (map_p.y - Pcirc3_y2) * (map_p.y - Pcirc3_y2))
					< 30) {
				InitChart(chart, "ppm", "godz.",
						"Lotne zw. org.- hist. pomiarow", last24val, VOC,
						current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplayMainScreen(MainScreen);
			break;
		case PART_03DetailScr:
			if (sqrt(
					(map_p.x - Pcirc2_x2) * (map_p.x - Pcirc2_x2)
							+ (map_p.y - Pcirc2_y2) * (map_p.y - Pcirc2_y2))
					< 30) {
				InitChart(chart, "n/0.1L", "godz.",
						"Czastki <0.3um- hist. pomiarow", last24val, PART_03,
						current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplaySecondScreen(MainScreen);
			break;
		case PART_05DetailScr:
			if (sqrt(
					(map_p.x - Pcirc2_x2) * (map_p.x - Pcirc2_x2)
							+ (map_p.y - Pcirc2_y2) * (map_p.y - Pcirc2_y2))
					< 30) {
				InitChart(chart, "n/0.1L", "godz.",
						"Czastki <0.5um- hist. pomiarow", last24val, PART_05,
						current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplaySecondScreen(MainScreen);
			break;
		case PART_1DetailScr:
			if (sqrt(
					(map_p.x - Pcirc2_x2) * (map_p.x - Pcirc2_x2)
							+ (map_p.y - Pcirc2_y2) * (map_p.y - Pcirc2_y2))
					< 30) {
				InitChart(chart, "n/0.1L", "godz.",
						"Czastki <1um- hist. pomiarow", last24val, PART_1,
						current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplaySecondScreen(MainScreen);
			break;
		case PART_2_5DetailScr:
			if (sqrt(
					(map_p.x - Pcirc2_x2) * (map_p.x - Pcirc2_x2)
							+ (map_p.y - Pcirc2_y2) * (map_p.y - Pcirc2_y2))
					< 30) {
				InitChart(chart, "n/0.1L", "godz.",
						"Czastki <2.5um- hist. pomiarow", last24val, PART_2_5,
						current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplaySecondScreen(MainScreen);
			break;
		case PART_5DetailScr:
			if (sqrt(
					(map_p.x - Pcirc2_x2) * (map_p.x - Pcirc2_x2)
							+ (map_p.y - Pcirc2_y2) * (map_p.y - Pcirc2_y2))
					< 30) {
				InitChart(chart, "n/0.1L", "godz.",
						"Czastki <5um- hist. pomiarow", last24val, PART_5,
						current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplaySecondScreen(MainScreen);
			break;
		case PART_10DetailScr:
			if (sqrt(
					(map_p.x - Pcirc2_x2) * (map_p.x - Pcirc2_x2)
							+ (map_p.y - Pcirc2_y2) * (map_p.y - Pcirc2_y2))
					< 30) {
				InitChart(chart, "n/0.1L", "godz.",
						"Czastki <10um- hist. pomiarow", last24val, PART_10,
						current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplaySecondScreen(MainScreen);
			break;
		case LPGDetailScr:
			if (sqrt(
					(map_p.x - Pcirc2_x2) * (map_p.x - Pcirc2_x2)
							+ (map_p.y - Pcirc2_y2) * (map_p.y - Pcirc2_y2))
					< 30) {
				InitChart(chart, "%", "godz.", "LPG- hist. pomiarow",
						last24val, LPG, current_h);
				screen = DisplayChartArea(chart, Line,24);
				break;
			}
			if (map_p.x > DETAIL_BCK_BTN_X
					&& map_p.x < DETAIL_BCK_BTN_X + DETAIL_BCK_BTN_W
					&& map_p.y > DETAIL_BCK_BTN_Y
					&& map_p.y < DETAIL_BCK_BTN_Y + DETAIL_BCK_BTN_H)
				screen = DisplaySecondScreen(MainScreen);
			break;
		default:
			screen = current;
			break;
		}

		return screen;
	} else
		return current;
}

CurrentScreen ChartScrTouchDetect(Chart* chart, CurrentScreen current,
		CircleArea DetailScreen[][3]) {
	TSPoint p, map_p;
	CurrentScreen screen;
	screen = current;
	p = getPoint();

	if (p.z > MINPRES) {
		map_p = getScreenPoint(p);

		if (current == LineChartScr) {
			if (map_p.x > CHART_BUTTON1_X0
					&& map_p.x < CHART_BUTTON1_X0 + CHART_BUTTON_WIDTH
					&& map_p.y > CHART_BUTTON_Y
					&& map_p.y < CHART_BUTTON_Y + CHART_BUTTON_HIGHT)
				screen = ChartBackButtonPress(chart, DetailScreen);
			if (map_p.x > CHART_BUTTON2_X0
					&& map_p.x < CHART_BUTTON2_X0 + CHART_BUTTON_WIDTH
					&& map_p.y > CHART_BUTTON_Y
					&& map_p.y < CHART_BUTTON_Y + CHART_BUTTON_HIGHT)
				screen = current;
			if (map_p.x > CHART_BUTTON3_X0
					&& map_p.x < CHART_BUTTON3_X0 + CHART_BUTTON_WIDTH
					&& map_p.y > CHART_BUTTON_Y
					&& map_p.y < CHART_BUTTON_Y + CHART_BUTTON_HIGHT)
				screen = DisplayChartArea(chart, Bar,24);
			if (map_p.x > CHART_BUTTON_HOUR6_X0+5
					&& map_p.x < CHART_BUTTON_HOUR6_X0 + CHART_BUTTON_HOUR_WIDTH
					&& map_p.y > CHART_BUTTON_Y
					&& map_p.y < CHART_BUTTON_Y + CHART_BUTTON_HIGHT)
					screen = DisplayChartArea(chart, Line,6);
			if (map_p.x > CHART_BUTTON_HOUR12_X0+5
					&& map_p.x < CHART_BUTTON_HOUR12_X0 + CHART_BUTTON_HOUR_WIDTH
					&& map_p.y > CHART_BUTTON_Y
					&& map_p.y < CHART_BUTTON_Y + CHART_BUTTON_HIGHT)
				screen = DisplayChartArea(chart, Line,12);
			if (map_p.x > CHART_BUTTON_HOUR24_X0+5
					&& map_p.y > CHART_BUTTON_Y
					&& map_p.y < CHART_BUTTON_Y + CHART_BUTTON_HIGHT)
				screen = DisplayChartArea(chart, Line,24);

			return screen;
		} else {
			if (map_p.x > CHART_BUTTON1_X0
					&& map_p.x < CHART_BUTTON1_X0 + CHART_BUTTON_WIDTH
					&& map_p.y > CHART_BUTTON_Y
					&& map_p.y < CHART_BUTTON_Y + CHART_BUTTON_HIGHT)
				screen = ChartBackButtonPress(chart, DetailScreen);
			if (map_p.x > CHART_BUTTON2_X0
					&& map_p.x < CHART_BUTTON2_X0 + CHART_BUTTON_WIDTH
					&& map_p.y > CHART_BUTTON_Y
					&& map_p.y < CHART_BUTTON_Y + CHART_BUTTON_HIGHT)
				screen = DisplayChartArea(chart, Line,24);
			if (map_p.x > CHART_BUTTON3_X0
					&& map_p.x < CHART_BUTTON3_X0 + CHART_BUTTON_WIDTH
					&& map_p.y > CHART_BUTTON_Y
					&& map_p.y < CHART_BUTTON_Y + CHART_BUTTON_HIGHT)
				screen = current;
			if (map_p.x > CHART_BUTTON_HOUR6_X0
					&& map_p.x < CHART_BUTTON_HOUR6_X0 + CHART_BUTTON_HOUR_WIDTH
					&& map_p.y > CHART_BUTTON_Y
					&& map_p.y < CHART_BUTTON_Y + CHART_BUTTON_HIGHT)
				screen = DisplayChartArea(chart, Bar,6);
			if (map_p.x > CHART_BUTTON_HOUR12_X0
					&& map_p.x < CHART_BUTTON_HOUR12_X0 + CHART_BUTTON_HOUR_WIDTH
					&& map_p.y > CHART_BUTTON_Y
					&& map_p.y < CHART_BUTTON_Y + CHART_BUTTON_HIGHT)
				screen = DisplayChartArea(chart, Bar,12);
			if (map_p.x > CHART_BUTTON_HOUR24_X0
					&& map_p.x < CHART_BUTTON_HOUR24_X0 + CHART_BUTTON_HOUR_WIDTH
					&& map_p.y > CHART_BUTTON_Y
					&& map_p.y < CHART_BUTTON_Y + CHART_BUTTON_HIGHT)
				screen = DisplayChartArea(chart, Bar,24);

			return screen;
		}
	} else
		return current;
}

CurrentScreen SettingsScrTouchDetect(MainScrArea SecondScreen[],RTC_TimeTypeDef* time_to_set,RTC_DateTypeDef* date_to_set)
{
	CurrentScreen screen = SettingsScr;
	TSPoint p, map_p;

double sq ;
	p = getPoint();
	if (p.z > MINPRES)
	{
		map_p = getScreenPoint(p);
		sq=sqrt((map_p.x - (HOUR_SETT_X0+35)) * (map_p.x - (HOUR_SETT_X0+35))+ (map_p.y - (TIME_SETT_Y0-33)) * (map_p.y - (TIME_SETT_Y0-33)));
		if(sq < 30)
		{
			if(time_to_set < 24)
			time_to_set->Hours += 1;
			else
				time_to_set->Hours = 0;
			fillRect(HOUR_SETT_X0,TIME_SETT_Y0,SEV_SEGMENT_WIDTH*4,50,WHITE);
			drawNumber(time_to_set->Hours,HOUR_SETT_X0,TIME_SETT_Y0,7,WHITE);
		}
		else
		{
			sq=sqrt((map_p.x - (HOUR_SETT_X0+35)) * (map_p.x - (HOUR_SETT_X0+35))+ (map_p.y - (TIME_SETT_Y0+83)) * (map_p.y - (TIME_SETT_Y0+83)));
			if(sq <30 )
			{
				if(time_to_set > 0)
				time_to_set->Hours -= 1;
				else
					time_to_set->Hours = 23;
				fillRect(HOUR_SETT_X0,TIME_SETT_Y0,SEV_SEGMENT_WIDTH*4,50,WHITE);
				drawNumber(time_to_set->Hours,HOUR_SETT_X0,TIME_SETT_Y0,7,WHITE);
			}
			else
			{
				sq = sqrt((map_p.x - (MIN_SETT_X0+35)) * (map_p.x - (MIN_SETT_X0+35))+ (map_p.y - (TIME_SETT_Y0-33)) * (map_p.y - (TIME_SETT_Y0-33)));
				if(sq < 30)
				{
					if(time_to_set->Minutes < 60)
					time_to_set->Minutes += 1;
					else
						time_to_set->Minutes = 0;
					fillRect(MIN_SETT_X0,TIME_SETT_Y0,SEV_SEGMENT_WIDTH*4,50,WHITE);
					drawNumber(time_to_set->Minutes,MIN_SETT_X0,TIME_SETT_Y0,7,WHITE);
				}
				else
				{
					sq = sqrt((map_p.x - (MIN_SETT_X0+35)) * (map_p.x - (MIN_SETT_X0+35))+ (map_p.y - (TIME_SETT_Y0+83)) * (map_p.y - (TIME_SETT_Y0+83)));
					if(sq < 30)
					{
						if(time_to_set->Minutes > 0)
						time_to_set->Minutes -= 1;
						else
							time_to_set->Minutes = 59;
						fillRect(MIN_SETT_X0,TIME_SETT_Y0,SEV_SEGMENT_WIDTH*4,50,WHITE);
						drawNumber(time_to_set->Minutes,MIN_SETT_X0,TIME_SETT_Y0,7,WHITE);
					}
					else
					{
						sq = sqrt((map_p.x - (DAY_SETT_X0+35)) * (map_p.x - (DAY_SETT_X0+35))+ (map_p.y - (DATE_SETT_Y0-33)) * (map_p.y - (DATE_SETT_Y0-33)));
						if(sq < 30)
						{
							if(date_to_set->Date < 31 )
							date_to_set->Date += 1;
							else
								date_to_set = 1;
							fillRect(DAY_SETT_X0,DATE_SETT_Y0,SEV_SEGMENT_WIDTH*4,50,WHITE);
							drawNumber(date_to_set->Date,DAY_SETT_X0,DATE_SETT_Y0,7,WHITE);
						}
						else
						{
							sq = sqrt((map_p.x - (DAY_SETT_X0+35)) * (map_p.x - (DAY_SETT_X0+35))+ (map_p.y - (DATE_SETT_Y0+83)) * (map_p.y - (DATE_SETT_Y0+83)));
							if(sq < 30)
							{
								if(date_to_set->Date > 0)
								date_to_set->Date -= 1;
								else
									date_to_set->Date = 31;
								fillRect(DAY_SETT_X0,DATE_SETT_Y0,SEV_SEGMENT_WIDTH*4,50,WHITE);
								drawNumber(date_to_set->Date,DAY_SETT_X0,DATE_SETT_Y0,7,WHITE);
							}
							else
							{
								sq = sqrt((map_p.x - (MONTH_SETT_X0+35)) * (map_p.x - (MONTH_SETT_X0+35))+ (map_p.y - (DATE_SETT_Y0-33)) * (map_p.y - (DATE_SETT_Y0-33)));
								if(sq < 30)
								{
									if(date_to_set->Month < 12)
									date_to_set->Month += 1;
									else
										date_to_set->Month = 1;
									fillRect(MONTH_SETT_X0,DATE_SETT_Y0,SEV_SEGMENT_WIDTH*4,50,WHITE);
									drawNumber(date_to_set->Month,MONTH_SETT_X0,DATE_SETT_Y0,7,WHITE);
								}
								else
								{
									sq = sqrt((map_p.x - (MONTH_SETT_X0+35)) * (map_p.x - (MONTH_SETT_X0+35))+ (map_p.y - (DATE_SETT_Y0+83)) * (map_p.y - (DATE_SETT_Y0+83)));
									if(sq < 30)
									{
										if(date_to_set->Month > 0)
										date_to_set->Month -= 1;
										else
											date_to_set->Month = 12;
										fillRect(MONTH_SETT_X0,DATE_SETT_Y0,SEV_SEGMENT_WIDTH*4,50,WHITE);
										drawNumber(date_to_set->Month,MONTH_SETT_X0,DATE_SETT_Y0,7,WHITE);
									}
									else
									{
										sq = sqrt((map_p.x - (YEAR_SETT_X0+35)) * (map_p.x - (YEAR_SETT_X0+35))+ (map_p.y - (DATE_SETT_Y0-33)) * (map_p.y - (DATE_SETT_Y0-33)));
										if(sq < 30)
										{
											date_to_set->Year += 1;
											fillRect(YEAR_SETT_X0,DATE_SETT_Y0,SEV_SEGMENT_WIDTH*4,50,WHITE);
											drawNumber(date_to_set->Year,YEAR_SETT_X0,DATE_SETT_Y0,7,WHITE);
										}
										else
										{
											sq = sqrt((map_p.x - (YEAR_SETT_X0+35)) * (map_p.x - (YEAR_SETT_X0+35))+ (map_p.y - (DATE_SETT_Y0+83)) * (map_p.y - (DATE_SETT_Y0+83)));
											if(sq < 30)
											{
												date_to_set->Year -= 1;
												fillRect(YEAR_SETT_X0,DATE_SETT_Y0,SEV_SEGMENT_WIDTH*4,50,WHITE);
												drawNumber(date_to_set->Year,YEAR_SETT_X0,DATE_SETT_Y0,7,WHITE);
											}
											else
											{
												if(map_p.x > TIME_SET_BTN_X0 && map_p.x < TIME_SET_BTN_X0 + TIME_SET_BTN_W && map_p.y > TIME_SET_BTN_YO && map_p.y< TIME_SET_BTN_YO+TIME_SET_BTN_H)
												{
													HAL_RTC_SetTime(&hrtc, time_to_set, RTC_FORMAT_BCD);
													HAL_RTC_SetDate(&hrtc, date_to_set, RTC_FORMAT_BCD);
													screen = DisplaySecondScreen(SecondScreen);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}


	return screen;
}

void InitMainScrArea(MainScrArea* area, int x0, int y0, char areaName[],
		char unit[], int color, int yellow_alert, int red_alert, int if_float,
		int decimal) {
	area->w = 154;
	area->h = 92;
	area->r = 10;
	area->color = color;

	area->val_width = 152;
	area->val_hight = 50;

	area->value = -1;

	area->yellow = yellow_alert;
	area->red = red_alert;

	area->if_float = if_float;
	area->decimal = decimal;

	//Init name
	int n = charsInTable(areaName);
	int i;
	//Czyszczenie tablicy
	for (i = 0; i < sizeof(area->areaName); i++) {
		area->areaName[i] = '\0';
	}

	//Wpisanie danych
	for (i = 0; i < n; i++) {
		area->areaName[i] = areaName[i];

	}
	//Init unit
	SetMainScrAreaUnit(area, unit);

	//Area background start point
	area->x0 = x0 + 2;
	area->y0 = y0 + 2;

	//Title Area start point
	area->x1 = x0 + 6;
	area->y1 = y0 + 6;

	//Value start point
	/*	if(if_float)
	 area->x2 = x0 + 4;
	 else*/
	area->x2 = x0 + 52;

	area->y2 = y0 + 42;

	//Unit start point
	area->x3 = x0 + 67;
	area->y3 = y0 + 6;
}
void InitCircleArea(CircleArea* area, int x0, int y0, int r, char areaName[],
		char unit[], int color, int yellow_alert, int red_alert, int if_float,
		int decimal) {
	//Area background start point
	area->x0 = x0;
	area->y0 = y0;

	area->r = r;
	area->color = color;

	area->val_width = 129;
	area->val_hight = 50;

	area->value = -1;

	area->yellow = yellow_alert;
	area->red = red_alert;

	area->if_float = if_float;
	area->decimal = decimal;

	//Init name
	int n = charsInTable(areaName);
	int i;

	//Czyszczenie tablicy
	for (i = 0; i < sizeof(area->areaName); i++) {
		area->areaName[i] = '\0';
	}

	//Wpisanie danych
	for (i = 0; i < n; i++) {
		area->areaName[i] = areaName[i];

	}

	//Unit
	n = charsInTable(unit);
	//Czyszczenie tablicy
	for (i = 0; i < sizeof(area->unit); i++) {
		area->unit[i] = '\0';
	}

	//Wpisanie danych
	for (i = 0; i < n; i++) {
		area->unit[i] = unit[i];

	}
	//Title Area start point
	area->x1 = x0 + -50;
	area->y1 = y0 + 30;

	//Value start point
	area->x2 = x0 - 50;
	area->y2 = y0 - 50;

	//Unit start point
	switch (n) {
	case 1:
		area->x3 = x0 - 10;
		break;
	case 2:
		area->x3 = x0 - 15;
		break;
	case 3:
		area->x3 = x0 - 20;
		break;
	case 4:
		area->x3 = x0 - 25;
		break;
	case 5:
		area->x3 = x0 - 35;
		break;
	default:
		area->x3 = x0 - 35;
	}

	//area->x3 = x0 -35;
	area->y3 = y0;
}
void InitTitleScrArea(TitleArea* area, char areaName[]) {
	//Init name
	int n = charsInTable(areaName);
	int i;
	//Czyszczenie tablicy
	for (i = 0; i < TOPSCR_TITLE_MAX_CHAR_NUM; i++) {
		area->Title[i] = '\0';
	}

	//Wpisanie danych
	for (i = 0; i < n; i++) {
		area->Title[i] = areaName[i];

	}

	area->Sx = 2;
	area->Sy = 4;
	area->Ssize = 4;
	area->Scolor = WHITE;


	area->Tx = 410;
	area->Ty = 1;
	area->Tsize = 2;
	area->Tcolor = WHITE;

	area->Dx = 395;
	area->Dy = 15;
	area->Dsize = 2;
	area->Dcolor = WHITE;

}
void InitDetailedScreen(CircleArea DetScr[][3]) {
	//PM1_0
	InitCircleArea(&DetScr[PM1_0][0], Pcirc2_x1, Pcirc2_y1, DETAIL_CIRC_RADIUS,
			"Aktualny pomiar", "ug/m3", WHITE, 300, 500, 0, 0);
	InitCircleArea(&DetScr[PM1_0][1], Pcirc2_x2, Pcirc2_y2, DETAIL_CIRC_RADIUS,
			"Wart. sredniodob.", "ug/m3", WHITE, 300, 500, 0, 0);
	//InitCircleArea(&DetScr[PM1_0][2], Pcirc3_x3, Pcirc3_y3, DETAIL_CIRC_RADIUS,
	//
	//PM2_5
	InitCircleArea(&DetScr[PM2_5][0], Pcirc3_x1, Pcirc3_y1, DETAIL_CIRC_RADIUS,
			"Aktualny pomiar", "ug/m3", WHITE, 300, 500, 0, 0);
	InitCircleArea(&DetScr[PM2_5][1], Pcirc3_x2, Pcirc3_y2, DETAIL_CIRC_RADIUS,
			"Wart. sredniodob.", "ug/m3", WHITE, 300, 500, 0, 0);
	InitCircleArea(&DetScr[PM2_5][2], Pcirc3_x3, Pcirc3_y3, DETAIL_CIRC_RADIUS,
			"Przekrocz. normy", "%", WHITE, 300, 500, 0, 0);

	//PM10
	InitCircleArea(&DetScr[PM10][0], Pcirc3_x1, Pcirc3_y1, DETAIL_CIRC_RADIUS,
			"Aktualny pomiar", "ug/m3", WHITE, 300, 500, 0, 0);
	InitCircleArea(&DetScr[PM10][1], Pcirc3_x2, Pcirc3_y2, DETAIL_CIRC_RADIUS,
			"Wart. sredniodob.", "ug/m3", WHITE, 300, 500, 0, 0);
	InitCircleArea(&DetScr[PM10][2], Pcirc3_x3, Pcirc3_y3, DETAIL_CIRC_RADIUS,
			"Przekrocz. normy", "%", WHITE, 300, 500, 0, 0);

	//HUM
	InitCircleArea(&DetScr[HUM][0], Pcirc2_x1, Pcirc2_y1, DETAIL_CIRC_RADIUS + 25,
			"Aktualny pomiar", "%", WHITE, 300, 500, 1, 1);
	InitCircleArea(&DetScr[HUM][1], Pcirc2_x2, Pcirc2_y2, DETAIL_CIRC_RADIUS + 25,
			"Wart. sredniodob.", "%", WHITE, 300, 500, 1, 1);

	//CO2
	InitCircleArea(&DetScr[CO2][0], Pcirc3_x1, Pcirc3_y1, DETAIL_CIRC_RADIUS,
			"Aktualny pomiar", "ppm", WHITE, 300, 500, 0, 0);
	InitCircleArea(&DetScr[CO2][1], Pcirc3_x2, Pcirc3_y2, DETAIL_CIRC_RADIUS,
			"Wart. sredniodob.", "ppm", WHITE, 300, 500, 0, 0);
	InitCircleArea(&DetScr[CO2][2], Pcirc3_x3, Pcirc3_y3, DETAIL_CIRC_RADIUS,
			"Przekrocz. normy", "%", WHITE, 300, 500, 0, 0);

	//HCHO
	InitCircleArea(&DetScr[HCHO][0], Pcirc3_x1, Pcirc3_y1, DETAIL_CIRC_RADIUS,
			"Aktualny pomiar", "mg/m3", WHITE, 300, 500, 1, 2);
	InitCircleArea(&DetScr[HCHO][1], Pcirc3_x2, Pcirc3_y2, DETAIL_CIRC_RADIUS,
			"Wart. sredniodob.", "mg/m3", WHITE, 300, 500, 1, 2);
	InitCircleArea(&DetScr[HCHO][2], Pcirc3_x3, Pcirc3_y3, DETAIL_CIRC_RADIUS,
			"Przekrocz. normy", "%", WHITE, 300, 500, 1, 2);

	//Temp
	InitCircleArea(&DetScr[TEMP][0], Pcirc2_x1, Pcirc2_y1, DETAIL_CIRC_RADIUS + 25,
			"Aktualny pomiar", "st.C", WHITE, 300, 500, 1, 1);
	InitCircleArea(&DetScr[TEMP][1], Pcirc2_x2, Pcirc2_y2, DETAIL_CIRC_RADIUS + 25,
			"Wart. sredniodob.", "st.C", WHITE, 300, 500, 1, 1);

	//VOC
	InitCircleArea(&DetScr[VOC][0], Pcirc2_x1, Pcirc2_y1, DETAIL_CIRC_RADIUS,
			"Aktualny pomiar", "ppm", WHITE, 9999, 9999, 0, 0);
	InitCircleArea(&DetScr[VOC][1], Pcirc2_x2, Pcirc2_y2, DETAIL_CIRC_RADIUS,
			"Wart. sredniodob.", "ppm", WHITE, 9999, 9999, 0, 0);
	//InitCircleArea(&DetScr[VOC][2], Pcirc3_x3, Pcirc3_y3, DETAIL_CIRC_RADIUS,
	//		"Przekrocz. normy", "%", WHITE, 9999, 999, 0, 0);

	//PART <0.3um/0.1L
	InitCircleArea(&DetScr[PART_03][0], Pcirc2_x1, Pcirc2_y1, DETAIL_CIRC_RADIUS + 25,
			"Aktualny pomiar", "l.czast/0.1L", WHITE, 99999, 99999, 0, 0);
	InitCircleArea(&DetScr[PART_03][1], Pcirc2_x2, Pcirc2_y2, DETAIL_CIRC_RADIUS + 25,
			"Wart. sredniodob.", "l.czast/0.1L", WHITE, 99999, 99999, 0, 0);

	//PART <0.5um/0.1L
	InitCircleArea(&DetScr[PART_05][0], Pcirc2_x1, Pcirc2_y1, DETAIL_CIRC_RADIUS + 25,
			"Aktualny pomiar", "l.czast/0.1L", WHITE, 99999, 99999, 0, 0);
	InitCircleArea(&DetScr[PART_05][1], Pcirc2_x2, Pcirc2_y2, DETAIL_CIRC_RADIUS + 25,
			"Wart. sredniodob.", "l.czast/0.1L", WHITE, 99999, 99999, 0, 0);

	//PART <1um/0.1L
	InitCircleArea(&DetScr[PART_1][0], Pcirc2_x1, Pcirc2_y1, DETAIL_CIRC_RADIUS + 25,
			"Aktualny pomiar", "l.czast/0.1L", WHITE, 99999, 99999, 0, 0);
	InitCircleArea(&DetScr[PART_1][1], Pcirc2_x2, Pcirc2_y2, DETAIL_CIRC_RADIUS + 25,
			"Wart. sredniodob.", "l.czast/0.1L", WHITE, 99999, 99999, 0, 0);

	//PART <2.5um/0.1L
	InitCircleArea(&DetScr[PART_2_5][0], Pcirc2_x1, Pcirc2_y1, DETAIL_CIRC_RADIUS + 25,
			"Aktualny pomiar", "l.czast/0.1L", WHITE, 99999, 99999, 0, 0);
	InitCircleArea(&DetScr[PART_2_5][1], Pcirc2_x2, Pcirc2_y2, DETAIL_CIRC_RADIUS + 25,
			"Wart. sredniodob.", "l.czast/0.1L", WHITE, 99999, 99999, 0, 0);

	//PART <5um/0.1L
	InitCircleArea(&DetScr[PART_5][0], Pcirc2_x1, Pcirc2_y1, DETAIL_CIRC_RADIUS + 25,
			"Aktualny pomiar", "l.czast/0.1L", WHITE, 99999, 99999, 0, 0);
	InitCircleArea(&DetScr[PART_5][1], Pcirc2_x2, Pcirc2_y2, DETAIL_CIRC_RADIUS + 25,
			"Wart. sredniodob.", "l.czast/0.1L", WHITE, 99999, 99999, 0, 0);

	//PART <10um/0.1L
	InitCircleArea(&DetScr[PART_10][0], Pcirc2_x1, Pcirc2_y1, DETAIL_CIRC_RADIUS + 25,
			"Aktualny pomiar", "l.czast/0.1L", WHITE, 99999, 99999, 0, 0);
	InitCircleArea(&DetScr[PART_10][1], Pcirc2_x2, Pcirc2_y2, DETAIL_CIRC_RADIUS + 25,
			"Wart. sredniodob.", "l.czast/0.1L", WHITE, 99999, 99999, 0, 0);

	//LPG
	InitCircleArea(&DetScr[LPG][0], Pcirc2_x1, Pcirc2_y1, DETAIL_CIRC_RADIUS + 25,
			"Aktualny pomiar", "%", WHITE, 99999, 99999, 0, 0);
	InitCircleArea(&DetScr[LPG][1], Pcirc2_x2, Pcirc2_y2, DETAIL_CIRC_RADIUS + 25,
			"Wart. sredniodob.", "%", WHITE, 99999, 99999, 0, 0);

}

void InitMainScreen(MainScrArea MainScreen[]) {
	//Init all main areas
	InitMainScrArea(&MainScreen[0], P1x, P1y, "PM1.0", "ug/m3", GREEN, 50, 150,
			0, 0);
	InitMainScrArea(&MainScreen[1], P2x, P2y, "PM2.5", "ug/m3", GREEN, 100, 250,
			0, 0);
	InitMainScrArea(&MainScreen[2], P3x, P3y, "PM10", "ug/m3", GREEN, 200, 300,
			0, 0);
	InitMainScrArea(&MainScreen[3], P4x, P4y, "Wilg.", "%", GREEN, 95, 100, 1,
			1);
	InitMainScrArea(&MainScreen[4], P5x, P5y, "CO2", "ppm", GREEN, 100, 150, 0,
			0);
	InitMainScrArea(&MainScreen[5], P6x, P6y, "HCHO", "mg/m3", GREEN, 50, 100,
			1, 2);
	InitMainScrArea(&MainScreen[6], P7x, P7y, "Temp.", "st.C", GREEN, 35, 50, 1,
			1);
	InitMainScrArea(&MainScreen[7], P8x, P8y, "LZO", "ppm", GREEN, 999, 999, 0,
			0);
	InitMainScrArea(&MainScreen[8], P9x, P9y, "Wiecej", "", GREY, 999, 999, 0,
			0);
}

void InitSecondScreen(MainScrArea SecondScreen[]) {
	//Init all second areas
	InitMainScrArea(&SecondScreen[0], P1x, P1y, "Czast.", "<0.3um/0.1L", GREEN,
			999, 999, 0, 0);
	InitMainScrArea(&SecondScreen[1], P2x, P2y, "Czast.", "<0.5um/0.1L", GREEN,
			999, 999, 0, 0);
	InitMainScrArea(&SecondScreen[2], P3x, P3y, "Czast.", "<1um/0.1L", GREEN,
			999, 999, 0, 0);
	InitMainScrArea(&SecondScreen[3], P4x, P4y, "Czast.", "<2.5um/0.1L", GREEN,
			999, 999, 0, 0);
	InitMainScrArea(&SecondScreen[4], P5x, P5y, "Czast.", "<5um/0.1L", GREEN,
			999, 999, 0, 0);
	InitMainScrArea(&SecondScreen[5], P6x, P6y, "Czast.", "<10um/0.1L", GREEN,
			999, 999, 0, 0);
	InitMainScrArea(&SecondScreen[6], P7x, P7y, "LPG.", "%", GREEN, 999, 999, 0,
			0);
	InitMainScrArea(&SecondScreen[7], P8x, P8y, "Ustawienia", "", GREEN, 999,
			999, 0, 0);
	InitMainScrArea(&SecondScreen[8], P9x, P9y, "Cofnij", "", GREY, 999, 999, 0,
			0);
}

void DrawMainScrArea(MainScrArea* area) {
	//Background rectangle
	fillRoundRect(area->x0, area->y0, area->w, area->h, area->r, area->color);
	//Frame of background rectangle
	drawRoundRect(area->x0, area->y0, area->w, area->h, area->r, BLACK);
	//Display area name
	drawString(area->areaName, area->x1, area->y1, 4, area->color);
	//Display value
	if(area->value != -1)
	DrawMainScrAreaValue(area);

	//Display unit
	drawString(area->unit, area->x3 + 12, area->y3 + 5, 2, area->color);
}

void DrawCircleArea(CircleArea* area) {

	fillCircle(area->x0, area->y0, area->r + 5, GREEN);
	fillCircle(area->x0, area->y0, area->r, area->color);

	//Display area name
	drawString(area->areaName, area->x1, area->y1, 2, area->color);
	//Display value
	if(area->value != -1)
	DrawCircleAreaValue(area);


	//Display unit
	drawString(area->unit, area->x3, area->y3, 4, area->color);

}
void DrawTitleArea(TitleArea* area) {
	//rysowanie linii u góry ekranu
	drawFastHLine(0, 31, WIDTH, BLUE);
	drawFastHLine(0, 32, WIDTH, BLUE);

	//Tekst po lewej stronie
	drawString(area->Title, area->Sx, area->Sy, area->Ssize, area->Scolor);

}
CurrentScreen DisplayMainScreen(MainScrArea MainScreen[]) {
	fillScreen(HX8357_WHITE);
	TitleArea titlearea;
	InitTitleScrArea(&titlearea, "Aktualne pomiary - 1");
	DrawTitleArea(&titlearea);

	//Wyświet;anie aktualnej godziny i daty
	RTC_TimeTypeDef current_time;
    RTC_DateTypeDef current_date;
 	char time[9];
 	char date[11];

	HAL_RTC_GetTime(&hrtc, &current_time, RTC_FORMAT_BCD);
	HAL_RTC_GetDate(&hrtc, &current_date, RTC_FORMAT_BCD);
	sprintf(time, "%02u:%02u", current_time.Hours,current_time.Minutes);
	drawString(time,410,1,2,WHITE);
	sprintf(date, "%02u-%02u-%04u",current_date.Date ,current_date.Month,current_date.Year+2000);
	drawString(date,395,15,2,WHITE);

	//Rysowanie kolejnych stref
	int i;
	for (i = 0; i < MAIN_SCR_MAX_AREAS; i++) {
		DrawMainScrArea(&MainScreen[i]);
	}

	return MainScr;
}
CurrentScreen DisplaySecondScreen(MainScrArea SecondScreen[]) {
	fillScreen(HX8357_WHITE);

	TitleArea TitleScr;
	InitTitleScrArea(&TitleScr, "Aktualne pomiary - 2");
	DrawTitleArea(&TitleScr);

	//Wyświet;anie aktualnej godziny i daty
	RTC_TimeTypeDef current_time;
    RTC_DateTypeDef current_date;
 	char time[9];
 	char date[11];

	HAL_RTC_GetTime(&hrtc, &current_time, RTC_FORMAT_BCD);
	HAL_RTC_GetDate(&hrtc, &current_date, RTC_FORMAT_BCD);
	sprintf(time, "%02u:%02u", current_time.Hours,current_time.Minutes);
	drawString(time,410,1,2,WHITE);
	sprintf(date, "%02u-%02u-%04u",current_date.Date ,current_date.Month,current_date.Year+2000);
	drawString(date,395,15,2,WHITE);

	//Rysowanie kolejnych stref
	int i;
	for (i = 0; i < MAIN_SCR_MAX_AREAS; i++) {
		DrawMainScrArea(&SecondScreen[i]);
	}

	return SecondScr;
}

CurrentScreen DisplayDetailScreen(CircleArea DetailScreen[][3], char Title[],
		int index, int numofareas) {

	fillScreen(HX8357_WHITE);


	TitleArea TitleScr;
	InitTitleScrArea(&TitleScr, Title);
	DrawTitleArea(&TitleScr);

	//Wyświetanie aktualnej godziny i daty
	RTC_TimeTypeDef current_time;
    RTC_DateTypeDef current_date;
 	char time[9];
 	char date[11];

	HAL_RTC_GetTime(&hrtc, &current_time, RTC_FORMAT_BCD);
	HAL_RTC_GetDate(&hrtc, &current_date, RTC_FORMAT_BCD);
	sprintf(time, "%02u:%02u", current_time.Hours,current_time.Minutes);
	drawString(time,410,1,2,WHITE);
	sprintf(date, "%02u-%02u-%04u",current_date.Date ,current_date.Month,current_date.Year+2000);
	drawString(date,395,15,2,WHITE);

	int i;
	if (numofareas < 1 || numofareas > 3)
		return MainScr;

	for (i = 0; i < numofareas; i++) {
		DrawCircleArea(&DetailScreen[index][i]);
	}

	fillRoundRect(DETAIL_BCK_BTN_X, DETAIL_BCK_BTN_Y, DETAIL_BCK_BTN_W,
			DETAIL_BCK_BTN_H, 5, color565(160, 160, 160));
	fillRoundRect(DETAIL_BCK_BTN_X + 2, DETAIL_BCK_BTN_Y + 2,
			DETAIL_BCK_BTN_W - 4, DETAIL_BCK_BTN_H - 4, 5, WHITE);
	drawString("Cofnij", 20, 50, 4, WHITE);

	switch (index) {
	case PM1_0:
		return PM1_0DetailScr;
		break;
	case PM2_5:
		return PM2_5DetailScr;
		break;
	case PM10:
		return PM10DetailScr;
		break;
	case CO2:
		return CO2DetailScr;
	case HCHO:
		return HCHODetailScr;
		break;
	case TEMP:
		return TempDetailScr;
		break;
	case HUM:
		return HumDetailScr;
		break;
	case VOC:
		return VOCDetailScr;
	case PART_03:
		return PART_03DetailScr;
		break;
	case PART_05:
		return PART_05DetailScr;
		break;
	case PART_1:
		return PART_1DetailScr;
		break;
	case PART_2_5:
		return PART_2_5DetailScr;
		break;
	case PART_5:
		return PART_5DetailScr;
		break;
	case PART_10:
		return PART_10DetailScr;
		break;
	case LPG:
		return LPGDetailScr;
		break;
	default:
		return MainScr;
	}

}

CurrentScreen DisplaySettingsScreen(RTC_TimeTypeDef* time_to_set, RTC_DateTypeDef* date_to_set)
{
	fillScreen(WHITE);

	TitleArea TitleScr;
	InitTitleScrArea(&TitleScr, "Ustawienia czasu");
	DrawTitleArea(&TitleScr);

	//Wyświetanie aktualnej godziny i daty
 	char time[9];
 	char date[11];

	HAL_RTC_GetTime(&hrtc, time_to_set, RTC_FORMAT_BCD);
	HAL_RTC_GetDate(&hrtc, date_to_set, RTC_FORMAT_BCD);
	sprintf(time, "%02u:%02u", time_to_set->Hours,time_to_set->Minutes);
	drawString(time,410,1,2,WHITE);
	sprintf(date, "%02u-%02u-%04u",date_to_set->Date ,date_to_set->Month,date_to_set->Year+2000);
	drawString(date,395,15,2,WHITE);

	//Godzina
drawNumber(time_to_set->Hours,HOUR_SETT_X0,TIME_SETT_Y0,7,WHITE);
drawCircle(HOUR_SETT_X0+35,TIME_SETT_Y0-33,32,GREEN);//plus circ
fillRoundRect(HOUR_SETT_X0+35-32+2,TIME_SETT_Y0-33-4,2*32-2,8,4,GREEN);//horizontal rec
fillRoundRect(HOUR_SETT_X0+35-2,TIME_SETT_Y0-33-32+2,8,2*32-2,4,GREEN);//vertical rec
drawString("godz.",HOUR_SETT_X0,230,4,WHITE);

drawCircle(HOUR_SETT_X0+35,TIME_SETT_Y0+83,32,RED);//minus circ
fillRoundRect(HOUR_SETT_X0+35-32+2,TIME_SETT_Y0+83-4,2*32-2,8,4,RED);//horizontal rec

//Minuty
drawNumber(time_to_set->Minutes,MIN_SETT_X0,TIME_SETT_Y0,7,WHITE);
drawCircle(MIN_SETT_X0+35,TIME_SETT_Y0-33,32,GREEN);//plus circ
fillRoundRect(MIN_SETT_X0+35-32+2,TIME_SETT_Y0-33-4,2*32-2,8,4,GREEN);//horizontal rec
fillRoundRect(MIN_SETT_X0+35-2,TIME_SETT_Y0-33-32+2,8,2*32-2,4,GREEN);//vertical rec
drawString("min",MIN_SETT_X0,230,4,WHITE);

drawCircle(MIN_SETT_X0+35,TIME_SETT_Y0+83,32,RED);//minus circ
fillRoundRect(MIN_SETT_X0+35-32+2,TIME_SETT_Y0+83-4,2*32-2,8,4,RED);//horizontal rec

//Data
//Dzien
drawNumber(date_to_set->Date,DAY_SETT_X0,DATE_SETT_Y0,7,WHITE);
drawCircle(DAY_SETT_X0+35,DATE_SETT_Y0-33,32,GREEN);//plus circ
fillRoundRect(DAY_SETT_X0+35-32+2,DATE_SETT_Y0-33-4,2*32-2,8,4,GREEN);//horizontal rec
fillRoundRect(DAY_SETT_X0+35-2,DATE_SETT_Y0-33-32+2,8,2*32-2,4,GREEN);//vertical rec
drawString("dzien",DAY_SETT_X0,230,4,WHITE);

drawCircle(DAY_SETT_X0+35,DATE_SETT_Y0+83,32,RED);//minus circ
fillRoundRect(DAY_SETT_X0+35-32+2,DATE_SETT_Y0+83-4,2*32-2,8,4,RED);//horizontal rec
//Miesiac
drawNumber(date_to_set->Month,MONTH_SETT_X0,DATE_SETT_Y0,7,WHITE);
drawCircle(MONTH_SETT_X0+35,DATE_SETT_Y0-33,32,GREEN);//plus circ
fillRoundRect(MONTH_SETT_X0+35-32+2,DATE_SETT_Y0-33-4,2*32-2,8,4,GREEN);//horizontal rec
fillRoundRect(MONTH_SETT_X0+35-2,DATE_SETT_Y0-33-32+2,8,2*32-2,4,GREEN);//vertical rec
drawString("mies.",MONTH_SETT_X0,230,4,WHITE);

drawCircle(MONTH_SETT_X0+35,DATE_SETT_Y0+83,32,RED);//minus circ
fillRoundRect(MONTH_SETT_X0+35-32+2,DATE_SETT_Y0+83-4,2*32-2,8,4,RED);//horizontal rec

//Rok
drawNumber(date_to_set->Year,YEAR_SETT_X0,DATE_SETT_Y0,7,WHITE);
drawCircle(YEAR_SETT_X0+35,DATE_SETT_Y0-33,32,GREEN);//plus circ
fillRoundRect(YEAR_SETT_X0+35-32+2,DATE_SETT_Y0-33-4,2*32-2,8,4,GREEN);//horizontal rec
fillRoundRect(YEAR_SETT_X0+35-2,DATE_SETT_Y0-33-32+2,8,2*32-2,4,GREEN);//vertical rec
drawString("rok",YEAR_SETT_X0,230,4,WHITE);

drawCircle(YEAR_SETT_X0+35,DATE_SETT_Y0+83,32,RED);//minus circ
fillRoundRect(YEAR_SETT_X0+35-32+2,DATE_SETT_Y0+83-4,2*32-2,8,4,RED);//horizontal rec

fillRoundRect(TIME_SET_BTN_X0,TIME_SET_BTN_YO,TIME_SET_BTN_W,TIME_SET_BTN_H,5,GREY);
drawString("Zapisz i wyjdz",TIME_SET_BTN_X0+10,TIME_SET_BTN_YO+10,4,GREY);

return SettingsScr;
}

int charsInTable(char tab[]) {
	int counter = 0;
	while (tab[counter] != '\0') {
		counter += 1;
	}
	return counter;
}
void ClearCharTable(char table[], int size) {
	int i;
	//Czyszczenie tablicy
	for (i = 0; i < size; i++) {
		table[i] = '\0';
	}
}

void CopyCharTable(char from[], char to[], int destsize) {
	int i;
	//Czyszczenie tablicy
	for (i = 0; i < destsize; i++) {
		to[i] = '\0';
	}

	int n = charsInTable(from);
	//Wpisanie danych
	for (i = 0; i < n; i++) {
		to[i] = from[i];

	}
}

void ChangeMainScrAreaColor(MainScrArea* area, int color) {
	if (color != area->color) {
		area->color = color;
		DrawMainScrArea(area);
	}
}

int SetMainScrAreaValue(MainScrArea* area, float value) {
	//Zamiana wartosci tylko jesli sie zmienila
	if (area->if_float == 0)
	{
		if (area->value - value < -1 || area->value - value > 1) {
			area->value = value;
			return 1;
		} else
			return 0;
	}
	else
	{
		if (area->decimal == 1) {
			if (area->value - value < -0.1 || area->value - value > 0.1) {
				area->value = value;
				return 1;
			} else
				return 0;
		}
		else
		{
			if (area->value - value < -0.001 || area->value - value > 0.001) {
				area->value = value;
				return 1;
			} else
				return 0;
		}
	}

}

void DrawMainScrAreaValue(MainScrArea* area) {
	if (area->if_float == 1) {
		if (area->value < 10) {
			fillRoundRect(area->x0 + 1, area->y2, area->w - 2, area->val_hight,
					5, area->color);
			if (area->decimal == 1)
				drawFloat(area->value, area->decimal, area->x2, area->y2, 7,
						area->color);
			else
				drawFloat(area->value, area->decimal,
						area->x2 - 3 * SEV_SEGMENT_WIDTH, area->y2, 7,
						area->color);
		} else if (area->value < 100) {
			fillRoundRect(area->x0 + 1, area->y2, area->w - 2, area->val_hight,
					5, area->color);
			drawFloat(area->value, area->decimal, area->x2 - SEV_SEGMENT_WIDTH,
					area->y2, 7, area->color);

		} else if (area->value < 1000) {
			fillRoundRect(area->x0 + 1, area->y2, area->w - 2, area->val_hight,
					5, area->color);
			drawFloat(area->value, area->decimal,
					area->x2 - 3 * SEV_SEGMENT_WIDTH, area->y2, 7, area->color);

		} else
			{
			fillRoundRect(area->x0 + 1, area->y2, area->w - 2, area->val_hight,
					5, area->color);
			drawString("ERR", area->x2 + SEV_SEGMENT_WIDTH, area->y2, 4,
					area->color);

		}

	}
	else //niefloat
	{
		if (area->value < 10) {
			fillRoundRect(area->x0 + 1, area->y2, area->w - 2, area->val_hight,
					5, area->color);
			drawNumber((int)area->value, area->x2 + SEV_SEGMENT_WIDTH, area->y2, 7,
					area->color);
		} else if (area->value < 100) {
			fillRoundRect(area->x0 + 1, area->y2, area->w - 2, area->val_hight,
					5, area->color);
			drawNumber((int)area->value, area->x2, area->y2, 7, area->color);
		}

		else if (area->value < 1000) {
			fillRoundRect(area->x0 + 1, area->y2, area->w - 2, area->val_hight,
					5, area->color);
			drawNumber((int)area->value, area->x2 - SEV_SEGMENT_WIDTH, area->y2, 7,
					area->color);
		} else if (area->value < 10000) {
			fillRoundRect(area->x0 + 1, area->y2, area->w - 2, area->val_hight,
					5, area->color);
			drawNumber((int)area->value, area->x2 - 3 * SEV_SEGMENT_WIDTH,
					area->y2, 7, area->color);
		} else {
			fillRoundRect(area->x0 + 1, area->y2, area->w - 2, area->val_hight,
					5, area->color);
			drawString("ERR", area->x2 + SEV_SEGMENT_WIDTH * 2, area->y2, 4,
					area->color);
		}

	}

}

int SetCircleScrAreaValue(CircleArea* area, float value) {
	//Zamiana wartosci tylko jesli wartosc sie zmienila..
	if (area->value - value < -0.01 || area->value - value > 0.01) {
		area->value = value;
		return 1;
	}
	else
		return 0;
}

void DrawCircleAreaValue(CircleArea* area) {
	if (area->if_float == 1) {
		if (area->value < 99.9) {

			fillRoundRect(area->x2 - 14, area->y2, area->val_width,
					area->val_hight, 5, area->color);
			if(area->decimal == 1)
			drawFloat(area->value, area->decimal, area->x2, area->y2, 7,
					area->color);
			else drawFloat(area->value, area->decimal, (area->x2)-SEV_SEGMENT_WIDTH, area->y2, 7,
					area->color);
		} else {
			if (area->value < 1000) {
				fillRoundRect(area->x2 - 14, area->y2, area->val_width,
						area->val_hight, 5, area->color);
				drawFloat(area->value, area->decimal,
						(area->x2) - SEV_SEGMENT_WIDTH, area->y2, 7,
						area->color);
			} else {
				fillRoundRect(area->x2 - 14, area->y2, area->val_width,
						area->val_hight, 5, area->color);
				drawString("ERR", area->x2, area->y2, 4, RED);
			}

		}
	}
	else {
		if (area->value < 10) {
			fillRoundRect(area->x2 - 14, area->y2, area->val_width,
					area->val_hight, 5, area->color);
			drawFloat(area->value, 0, area->x2 + 2 * SEV_SEGMENT_WIDTH - 8,
					area->y2, 7, area->color);
		} else if (area->value < 100) {
			fillRoundRect(area->x2 - 14, area->y2, area->val_width,
					area->val_hight, 5, area->color);
			drawFloat(area->value, 0, area->x2 + SEV_SEGMENT_WIDTH, area->y2, 7,
					area->color);
		} else if (area->value < 1000) {
			fillRoundRect(area->x2 - 14, area->y2, area->val_width,
					area->val_hight, 5, area->color);
			drawFloat(area->value, 0, area->x2, area->y2, 7, area->color);
		} else if (area->value < 10000) {
			fillRoundRect(area->x2 - 14, area->y2, area->val_width,
					area->val_hight, 5, area->color);
			drawFloat(area->value, 0, area->x2 - SEV_SEGMENT_WIDTH, area->y2, 7,
					area->color);
		} else {
			fillRoundRect(area->x2 - 14, area->y2, area->val_width,
					area->val_hight, 5, area->color);
			drawString("ERR", area->x2 + SEV_SEGMENT_WIDTH * 2, area->y2, 4,
					WHITE);
		}

	}

}

void SetMainScrAreaThresholds(MainScrArea* area, int yellow, int red) {
	area->yellow = yellow;
	area->red = red;
}

void SetMainScrAreaUnit(MainScrArea* area, char unit[]) {
	int i;
	//Czyszczenie tablicy
	for (i = 0; i < sizeof(area->unit); i++) {
		area->unit[i] = '\0';
	}

	//Init unit
	int n;
	n = charsInTable(unit);
	if (n > 0) {
		//area->unit[0] = '[';
		for (i = 0; i < n; i++) {
			area->unit[i] = unit[i];

		}
		//area->unit[i + 1] = ']';
	}
}

CurrentScreen RefreshMainScreen(CurrentScreen current, MainScrArea MainScreen[],
		volatile float AllValues[][3], MainScrArea SecondScreen[],
		CircleArea DetailScreen[][3]) {
	CurrentScreen screen;
	screen = current;
	MeasureType types[8];
	int i, N;
	if (current == MainScr) {
		MeasureType MainScrTypes[] = { PM1_0, PM2_5, PM10, HUM, CO2, HCHO, TEMP,
				VOC };
		N = MAIN_SCR_MAX_AREAS - 1;
		for (i = 0; i < N; i++) {
			types[i] = MainScrTypes[i];
		}

	} else if (current == SecondScr) {
		MeasureType SecondScrTypes[] = { PART_03, PART_05, PART_1, PART_2_5,
				PART_5, PART_10, LPG };
		N = MAIN_SCR_MAX_AREAS - 2;
		for (i = 0; i < N; i++) {
			types[i] = SecondScrTypes[i];
		}
	}

	for (i = 0; i < N; i++) {
		if (SetMainScrAreaValue(&MainScreen[i], AllValues[types[i]][0])) {
			DrawMainScrAreaValue(&MainScreen[i]);

			if (MainScreen[i].value >= MainScreen[i].yellow
					&& MainScreen[i].value < MainScreen[i].red)
				ChangeMainScrAreaColor(&MainScreen[i], YELLOW);
			else if (MainScreen[i].value >= MainScreen[i].red)
				ChangeMainScrAreaColor(&MainScreen[i], RED);
			else
				ChangeMainScrAreaColor(&MainScreen[i], GREEN);
		}
		if (current == MainScr)
			screen = MainScrTouchDetect(current, DetailScreen, SecondScreen);

		if(current == SecondScr)
			screen = SecondScrTouchDetect(current, DetailScreen, SecondScreen);


		if (screen != current)
			return screen;
	}

	return current;
}

void RefreshDetailScreen(MeasureType measure_type, CircleArea area[][3],
		volatile float values[][3]) {
	int i;
	for (i = 0; i < CIRC_SCR_MAX_AREAS; i++) {
		if (SetCircleScrAreaValue(&area[measure_type][i],
				values[measure_type][i]))
			DrawCircleAreaValue(&area[measure_type][i]);
	}

}

int Max(int tab[], int size) {
	int i;
	int max;
	max = tab[0]; // najpierw element max przyjmujemy wartość pierwszego elementu tablicy
	for (i = 0; i < size; i++) // pozniej sprawdzamy iliteracujnie , czy kolejny element tablicy jest większy od aktalnego max,
		if (tab[i] > max)    // jesli tak
			max = tab[i];

	return max;
}

//Charts functions
void InitChart(Chart* chart, char y_unit[], char x_unit[], char title[],
		int y_values[][24], MeasureType index, int current_hour) {

	//Set type of measurement considered with this chart
	chart->measure = index;

	//Set title of chart area
	CopyCharTable(title, chart->title, 50);
	//Init axis units
	CopyCharTable(y_unit, chart->y_unit, AREA_UNIT_MAX_CHAR_NUM);
	CopyCharTable(x_unit, chart->x_unit, AREA_UNIT_MAX_CHAR_NUM);

	//Copy actual values to display
	int i;
	for (i = 0; i < CHART_Y_VALUES_MAX_NUM_3; i++) {
		chart->y_values[i] = y_values[index][i];
	}

//Y and X axis span
	chart->y_span = CHART_Y_SPAN;
	chart->x_span = CHART_X_SPAN;

//Findind max and min value to be displayed
	chart->max_y_value = Max(chart->y_values, CHART_Y_VALUES_MAX_NUM_3);
	chart->min_y_value = chart->max_y_value / 10;
		if(chart->min_y_value == 0)
			chart->min_y_value = 1;
	chart->current_hour = current_hour;

}

void DisplayLineChart(Chart* chart, int numofhours) {

	//Y Axis
	//Y axis lines and unit
	drawFastVLine(CHART_X_Y_AXIS_X0, CHART_Y_AXIS_Y0, CHART_Y_AXIS_LEN, BLACK);
	drawLine(CHART_X_Y_AXIS_X0 - 2, CHART_Y_AXIS_Y0 + 5, CHART_X_Y_AXIS_X0,
			CHART_Y_AXIS_Y0, BLACK);
	drawLine(CHART_X_Y_AXIS_X0 + 2, CHART_Y_AXIS_Y0 + 5, CHART_X_Y_AXIS_X0,
			CHART_Y_AXIS_Y0, BLACK);
	drawString(chart->y_unit, CHART_X_Y_AXIS_X0 - 11, CHART_Y_AXIS_Y0 - 22, 2,
			WHITE);

	//Y axis labels (10 lables)
	int i, j, n;
	drawNumber(0, CHART_Y_LABLES_X0, CHART_Y_LABLES_Y0, 2, BLACK);
	j = CHART_Y_LABLES_Y0 - CHART_Y_SPAN;
	n = chart->min_y_value;
	//Writing lables from min to max value
	for (i = 0; i < CHART_Y_LABLES_NUM; i++) {
		drawNumber(n, CHART_Y_LABLES_X0, j, 2, BLACK);
		j -= CHART_Y_SPAN;
		n += chart->min_y_value;
	}

	//X Axis
	drawFastHLine(CHART_X_Y_AXIS_X0, CHART_X_AXIS_Y0, CHART_X_AXIS_LEN, BLACK);
	drawLine(CHART_X_AXIS_LEN + CHART_X_Y_AXIS_X0 - 5, CHART_X_AXIS_Y0 - 2,
			CHART_X_AXIS_LEN + CHART_X_Y_AXIS_X0, CHART_X_AXIS_Y0, BLACK);
	drawLine(CHART_X_AXIS_LEN + CHART_X_Y_AXIS_X0 - 5, CHART_X_AXIS_Y0 + 2,
			CHART_X_AXIS_LEN + CHART_X_Y_AXIS_X0, CHART_X_AXIS_Y0, BLACK);
	drawString(chart->x_unit, CHART_X_AXIS_LEN + CHART_X_Y_AXIS_X0 - 15,
			CHART_X_AXIS_Y0 + 4, 2, WHITE);

	j = LINE_CHART_X_LABLES_X0;
	n = chart->current_hour - numofhours;//23;

	int h, chart_x_labels_num, chart_x_span,chart_x_span_val;
	if(numofhours == 6)
	{
		chart_x_labels_num = CHART_X_LABLES_NUM_2;
		chart_x_span = CHART_X_SPAN_1;
		chart_x_span_val = CHART_X_SPAN_VAL_1;
	}
	else
	{
		if(numofhours == 12)
		{
			chart_x_labels_num = CHART_X_LABLES_NUM;
			chart_x_span = CHART_X_SPAN;
			chart_x_span_val = CHART_X_SPAN_VAL_2;
		}
		else
		{
			chart_x_labels_num = CHART_X_LABLES_NUM;
			chart_x_span = CHART_X_SPAN;
			chart_x_span_val = CHART_X_SPAN_VAL_3;
		}
	}
	for (i = 0; i < chart_x_labels_num; i++) {

		if (n < 1)
			h = 24 + n;
		else
			h = n;

		drawNumber(h, j, CHART_X_AXIS_Y0 + 1, 2, BLACK);
		j += chart_x_span;
		if(numofhours == 6 || numofhours == 12)
		n += 1;
		else
			n += 2;
	}

	//Display values

	int x0, y0, y1;
	x0 = CHART_X_SPAN;

	for (i = 24 - numofhours; i < CHART_Y_VALUES_MAX_NUM_3 - 1; i++) {
		y0 = RecalculateValuetoChartPoint(chart->max_y_value,
				chart->y_values[i]);
		y1 = RecalculateValuetoChartPoint(chart->max_y_value,
				chart->y_values[i + 1]);
		//Points
		drawCircle(x0, y0, 2, RED);
		//Lines
		if (i < CHART_Y_VALUES_MAX_NUM_3 - 1)
			drawLine(x0, y0, x0 + chart_x_span_val, y1, BLUE);
		x0 += chart_x_span_val;
	}
	drawCircle(x0, y1, 2, RED);//added

}

void DisplayBarChart(Chart* chart, int numofhours) {

	//Y Axis
	//Y axis lines and unit
	drawFastVLine(CHART_X_Y_AXIS_X0, CHART_Y_AXIS_Y0, CHART_Y_AXIS_LEN, BLACK);
	drawLine(CHART_X_Y_AXIS_X0 - 2, CHART_Y_AXIS_Y0 + 5, CHART_X_Y_AXIS_X0,
			CHART_Y_AXIS_Y0, BLACK);
	drawLine(CHART_X_Y_AXIS_X0 + 2, CHART_Y_AXIS_Y0 + 5, CHART_X_Y_AXIS_X0,
			CHART_Y_AXIS_Y0, BLACK);
	drawString(chart->y_unit, CHART_X_Y_AXIS_X0 - 11, CHART_Y_AXIS_Y0 - 22, 2,
			WHITE);

	//Y axis labels (10 lables)
	int i, j, n;
	drawNumber(0, CHART_Y_LABLES_X0, CHART_Y_LABLES_Y0, 2, BLACK);
	j = CHART_Y_LABLES_Y0 - CHART_Y_SPAN;
	n = chart->min_y_value;
	//Writing lables from min to max value
	for (i = 0; i < CHART_Y_LABLES_NUM; i++) {
		drawNumber(n, CHART_Y_LABLES_X0, j, 2, BLACK);
		j -= CHART_Y_SPAN;
		n += chart->min_y_value;
	}

	//X Axis
	drawFastHLine(CHART_X_Y_AXIS_X0, CHART_X_AXIS_Y0, CHART_X_AXIS_LEN, BLACK);
	drawLine(CHART_X_AXIS_LEN + CHART_X_Y_AXIS_X0 - 5, CHART_X_AXIS_Y0 - 2,
			CHART_X_AXIS_LEN + CHART_X_Y_AXIS_X0, CHART_X_AXIS_Y0, BLACK);
	drawLine(CHART_X_AXIS_LEN + CHART_X_Y_AXIS_X0 - 5, CHART_X_AXIS_Y0 + 2,
			CHART_X_AXIS_LEN + CHART_X_Y_AXIS_X0, CHART_X_AXIS_Y0, BLACK);
	drawString(chart->x_unit, CHART_X_AXIS_LEN + CHART_X_Y_AXIS_X0 - 15,
			CHART_X_AXIS_Y0 + 4, 2, WHITE);

	j = BAR_CHART_X_LABELS_X0;
	n = chart->current_hour - numofhours  ;

	int h, chart_x_labels_num, chart_x_span,chart_x_span_val;
	if(numofhours == 6)
	{
		chart_x_labels_num = CHART_X_LABLES_NUM_2;
		chart_x_span = CHART_X_SPAN_1;
		chart_x_span_val = CHART_X_SPAN_VAL_1;
	}
	else
	{
		if(numofhours == 12)
		{
			chart_x_labels_num = CHART_X_LABLES_NUM;
			chart_x_span = CHART_X_SPAN;
			chart_x_span_val = CHART_X_SPAN_VAL_2;
		}
		else
		{
			chart_x_labels_num = CHART_X_LABLES_NUM;
			chart_x_span = CHART_X_SPAN;
			chart_x_span_val = CHART_X_SPAN_VAL_3;
		}
	}

	for (i = 0; i < chart_x_labels_num; i++) {

		if (n < 1)
			h = 24 + n;
		else
			h = n;
		if (h > 10)
			drawNumber(h, j, CHART_X_AXIS_Y0 + 1, 2, BLACK);
		else
			drawNumber(h, j + 4, CHART_X_AXIS_Y0 + 1, 2, BLACK);
		j += chart_x_span
				;
		if(numofhours == 6 || numofhours == 12)
		n += 1;
		else
			n += 2;
	}

	//Display values

	int x0, y0;
	x0 = CHART_X_SPAN + 10;

	for (i = 24 - numofhours; i < CHART_Y_VALUES_MAX_NUM_3; i++) {
		y0 = RecalculateValuetoChartPoint(chart->max_y_value,
				chart->y_values[i]);
		fillRect(x0, y0, 15, 265 - y0, BLUE);

		x0 += chart_x_span_val;
	}

}
int RecalculateValuetoChartPoint(int max, int val) {
	float resolution, MAX, dif;
	MAX = (float) max;
	resolution = MAX / CHART_Y_AXIS_LEN;
	dif = val / resolution;
	return CHART_X_AXIS_Y0 - (int) dif;
}

void InitButton(ChartButton* button, int x0, int y0, int w, int h,
		uint16_t color, char name[]) {
	button->x0 = x0;
	button->y0 = y0;
	button->width = w;
	button->hight = h;
	button->color = color;

	//Init name
	int n = charsInTable(name);
	int i;
	//Czyszczenie tablicy
	for (i = 0; i < sizeof(button->name); i++) {
		button->name[i] = '\0';
	}

	//Wpisanie danych
	for (i = 0; i < n; i++) {
		button->name[i] = name[i];

	}
}

void DisplayChartButton(ChartButton* button) {
	fillRoundRect(button->x0, button->y0, button->width, button->hight, 5,
			button->color);
	drawRoundRect(button->x0, button->y0, button->width, button->hight, 5,
			BLACK);
	drawString(button->name, button->x0 + 5, button->y0 + 6, 4, button->color);
}

void SelectChartButton(ChartButton* button) {
	drawRoundRect(button->x0, button->y0, button->width, button->hight, 5, RED);
	drawRoundRect(button->x0 + 1, button->y0 + 1, button->width - 2,
			button->hight - 2, 5, RED);
}
CurrentScreen DisplayChartArea(Chart* chart, int chart_type, int numofhours) {



	fillScreen(HX8357_WHITE);

	TitleArea TitleScr;
	InitTitleScrArea(&TitleScr, chart->title);
	DrawTitleArea(&TitleScr);

	//Wyświetlanie aktualnej godziny i daty
	RTC_TimeTypeDef current_time;
    RTC_DateTypeDef current_date;
 	char time[9];
 	char date[11];

	HAL_RTC_GetTime(&hrtc, &current_time, RTC_FORMAT_BCD);
	HAL_RTC_GetDate(&hrtc, &current_date, RTC_FORMAT_BCD);
	sprintf(time, "%02u:%02u", current_time.Hours,current_time.Minutes);
	drawString(time,410,1,2,WHITE);
	sprintf(date, "%02u-%02u-%04u",current_date.Date ,current_date.Month,current_date.Year+2000);
	drawString(date,395,15,2,WHITE);

	ChartButton BackBtn, LineBtn, BarBtn, Btn6h, Btn12h,Btn24h;

	if (chart_type)
		DisplayLineChart(chart,numofhours);
	else
		DisplayBarChart(chart,numofhours);
	//Buttons
	uint16_t color;
	color = color565(160, 160, 160);

	InitButton(&BackBtn, CHART_BUTTON1_X0, CHART_BUTTON_Y, CHART_BUTTON_WIDTH-20,
			CHART_BUTTON_HIGHT, color, "Cofnij");
	InitButton(&LineBtn, CHART_BUTTON2_X0, CHART_BUTTON_Y, CHART_BUTTON_WIDTH,
			CHART_BUTTON_HIGHT, color, "Liniowy");
	InitButton(&BarBtn, CHART_BUTTON3_X0, CHART_BUTTON_Y,
			CHART_BUTTON_WIDTH + 15, CHART_BUTTON_HIGHT, color, "Slupkowy");
	InitButton(&Btn6h,CHART_BUTTON_HOUR6_X0,CHART_BUTTON_Y,CHART_BUTTON_HOUR_WIDTH,
			CHART_BUTTON_HIGHT,color,"6h");
	InitButton(&Btn12h,CHART_BUTTON_HOUR12_X0,CHART_BUTTON_Y,CHART_BUTTON_HOUR_WIDTH,
			CHART_BUTTON_HIGHT,color,"12h");
	InitButton(&Btn24h,CHART_BUTTON_HOUR24_X0,CHART_BUTTON_Y,CHART_BUTTON_HOUR_WIDTH,
			CHART_BUTTON_HIGHT,color,"24h");

	DisplayChartButton(&BackBtn);
	DisplayChartButton(&LineBtn);
	DisplayChartButton(&BarBtn);
	DisplayChartButton(&Btn6h);
	DisplayChartButton(&Btn12h);
	DisplayChartButton(&Btn24h);

	switch(numofhours)
	{
	case 6:
		SelectChartButton(&Btn6h);
		break;
	case 12:
		SelectChartButton(&Btn12h);
	break;
	case 24:
		SelectChartButton(&Btn24h);
		break;
	}

	if (chart_type) {
		SelectChartButton(&LineBtn);
		return LineChartScr;
	} else {
		SelectChartButton(&BarBtn);
		return BarChartScr;
	}

}

CurrentScreen ChartBackButtonPress(Chart* chart, CircleArea DetailScreen[][3]) {
	CurrentScreen screen;
	switch (chart->measure) {
	case PM1_0:
		screen = DisplayDetailScreen(DetailScreen, "PM1.0", chart->measure, 3);
		break;
	case PM2_5:
		screen = DisplayDetailScreen(DetailScreen, "PM2.5 pyl zawieszony",
				PM2_5, 3);
		break;
	case PM10:
		screen = DisplayDetailScreen(DetailScreen, "PM10 pyl zawieszony", PM10,
				3);
		break;
	case HUM:
		screen = DisplayDetailScreen(DetailScreen, "Wilgotnosc", HUM, 2);
		break;
	case CO2:
		screen = DisplayDetailScreen(DetailScreen, "Dwutlenek wegla", CO2, 3);
		break;
	case HCHO:
		screen = DisplayDetailScreen(DetailScreen, "Formaldehyd", HCHO, 3);
		break;
	case TEMP:
		screen = DisplayDetailScreen(DetailScreen, "Temperatura", TEMP, 2);
		break;
	case VOC:
		screen = DisplayDetailScreen(DetailScreen, "Lotne zwiazki org.", VOC,
				3);
		break;
	case PART_03:
		screen = DisplayDetailScreen(DetailScreen,
				"Czast. o rozmiarze <0.3um w 0.1L", PART_03, 2);
		break;
	case PART_05:
		screen = DisplayDetailScreen(DetailScreen,
				"Czast. o rozmiarze <0.5um w 0.1L", PART_05, 2);
		break;
	case PART_1:
		screen = DisplayDetailScreen(DetailScreen,
				"Czast. o rozmiarze <1um w 0.1L", PART_1, 2);
		break;
	case PART_2_5:
		screen = DisplayDetailScreen(DetailScreen,
				"Czast. o rozmiarze < 2.5um w 0.1L", PART_2_5, 2);
		break;
	case PART_5:
		screen = DisplayDetailScreen(DetailScreen,
				"Czast. o rozmiarze <5um w 0.1L", PART_5, 2);
		break;
	case PART_10:
		screen = DisplayDetailScreen(DetailScreen,
				"Czast. o rozmiarze <10um w 0.1L", PART_10, 2);
		break;
	case LPG:
		screen = DisplayDetailScreen(DetailScreen, "LPG", LPG, 2);
		break;
	default:
		screen = MainScr;
	}

	return screen;
}
