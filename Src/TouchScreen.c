// Touch screen library with X Y and Z (pressure) readings as well
// as oversampling to avoid 'bouncing'
// (c) ladyada / adafruit
// Code under MIT License

/*
#include "Arduino.h"
#include "pins_arduino.h"
*/

#ifdef __AVR
  #include <avr/pgmspace.h>
#elif defined(ESP8266)
  #include <pgmspace.h>
#endif
#include "TouchScreen.h"

// increase or decrease the touchscreen oversampling. This is a little different than you make think:
// 1 is no oversampling, whatever data we get is immediately returned
// 2 is double-sampling and we only return valid data if both points are the same
// 3+ uses insert sort to get the median value.
// We found 2 is precise yet not too slow so we suggest sticking with it!

#define NUMSAMPLES 2


#if (NUMSAMPLES > 2)
static void insert_sort(int array[], uint8_t size) {
  uint8_t j;
  int save;
  
  for (int i = 1; i < size; i++) {
    save = array[i];
    for (j = i; j >= 1 && save < array[j - 1]; j--)
      array[j] = array[j - 1];
    array[j] = save; 
  }
}
#endif


int16_t map(int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

int adc_read(uint32_t channel)
{
 ADC_ChannelConfTypeDef adc_ch;
 adc_ch.Channel = channel;
 adc_ch.Rank = 1;
 adc_ch.SamplingTime = ADC_SAMPLETIME_3CYCLES;
 HAL_ADC_ConfigChannel(&hadc2, &adc_ch);

 HAL_ADC_Start(&hadc2);
 HAL_ADC_PollForConversion(&hadc2, 500);
    return HAL_ADC_GetValue(&hadc2);
}

void pinMode(GPIO_TypeDef* port, uint16_t pin,uint32_t mode)
{
	GPIO_InitTypeDef gpio;
	if(mode == INPUT)
	{
		gpio.Mode = GPIO_MODE_ANALOG;
		gpio.Pin = pin;
		HAL_GPIO_Init(port, &gpio);
	}
	else
	{
		gpio.Pin = pin;
		gpio.Mode = GPIO_MODE_OUTPUT_PP;
		gpio.Pull = GPIO_NOPULL;
		gpio.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(port, &gpio);

	}

}


TSPoint getPoint(void) {
  int x, y, z;
  int samples[NUMSAMPLES];
  uint8_t i, valid;

  valid = 1;



  pinMode(LCD_Yp_GPIO_Port,LCD_Yp_Pin, INPUT);
  pinMode(LCD_Ym_GPIO_Port,LCD_Ym_Pin, INPUT);
  pinMode(LCD_Xp_GPIO_Port,LCD_Xp_Pin, OUTPUT);
  pinMode(LCD_Xm_GPIO_Port,LCD_Xm_Pin, OUTPUT);

  HAL_GPIO_WritePin(LCD_Xp_GPIO_Port,LCD_Xp_Pin,GPIO_PIN_SET);
  HAL_GPIO_WritePin(LCD_Xm_GPIO_Port,LCD_Xm_Pin,GPIO_PIN_RESET);

  HAL_Delay(20);//delayMicroseconds(20); // Fast ARM chips need to allow voltages to settle


   for (i=0; i<NUMSAMPLES; i++) {
     samples[i] = adc_read(Yp);//analogRead(_yp);
   }

#if NUMSAMPLES > 2
   insert_sort(samples, NUMSAMPLES);
#endif
#if NUMSAMPLES == 2
   // Allow small amount of measurement noise, because capacitive
   // coupling to a TFT display's signals can induce some noise.
   if (samples[0] - samples[1] < -20 || samples[0] - samples[1] > 20) {
     valid = 0;
   } else {
     samples[1] = (samples[0] + samples[1]) >> 1; // average 2 samples
   }
#endif

   x = (4095-samples[NUMSAMPLES/2]);

   pinMode(LCD_Xp_GPIO_Port,LCD_Xp_Pin,INPUT);
   pinMode(LCD_Xm_GPIO_Port,LCD_Xm_Pin, INPUT);
   pinMode(LCD_Ym_GPIO_Port,LCD_Ym_Pin, OUTPUT);
   pinMode(LCD_Yp_GPIO_Port,LCD_Yp_Pin, OUTPUT);

   HAL_GPIO_WritePin(LCD_Ym_GPIO_Port,LCD_Ym_Pin,GPIO_PIN_RESET);
   HAL_GPIO_WritePin(LCD_Yp_GPIO_Port,LCD_Yp_Pin,GPIO_PIN_SET);
  

  HAL_Delay(20);// delayMicroseconds(20); // Fast ARM chips need to allow voltages to settle


   for (i=0; i<NUMSAMPLES; i++) {
     samples[i] = adc_read(Xm);
   }

#if NUMSAMPLES > 2
   insert_sort(samples, NUMSAMPLES);
#endif
#if NUMSAMPLES == 2
   // Allow small amount of measurement noise, because capacitive
   // coupling to a TFT display's signals can induce some noise.
   if (samples[0] - samples[1] < -40 || samples[0] - samples[1] > 40) {
     valid = 0;
   } else {
     samples[1] = (samples[0] + samples[1]) >> 1; // average 2 samples
   }
#endif

   y = (4095-samples[NUMSAMPLES/2]);

   // Set X+ to ground
   // Set Y- to VCC
   // Hi-Z X- and Y+

/*  pinMode(_xp, OUTPUT);
   pinMode(_yp, INPUT);*/

   pinMode(LCD_Xp_GPIO_Port,LCD_Xp_Pin,OUTPUT);
   pinMode(LCD_Yp_GPIO_Port,LCD_Yp_Pin,INPUT);

   HAL_GPIO_WritePin(LCD_Xp_GPIO_Port,LCD_Xp_Pin,GPIO_PIN_RESET);
   HAL_GPIO_WritePin(LCD_Ym_GPIO_Port,LCD_Ym_Pin,GPIO_PIN_SET);
   int z1 = adc_read(Xm);
   int z2 = adc_read(Yp);

   if (_rxplate != 0) {
     // now read the x 
     float rtouch;
     rtouch = z2;
     rtouch /= z1;
     rtouch -= 1;
     rtouch *= x;
     rtouch *= _rxplate;
     rtouch /= 4096;
     
     z = rtouch;
   } else {
     z = (4095-(z2-z1));
   }

   if (! valid) {
     z = 0;
   }

   TSPoint p;
   p.x = x;
   p.y = y;
   p.z = z;
   return p;
}

TSPoint getScreenPoint(TSPoint point)
{
	TSPoint mappoint;

	   mappoint.y = map(point.x, MINY, MAXX, 0, 320);
	   mappoint.x = map(point.y, MAXY, MINY, 0, 480);

	   return mappoint;
}

