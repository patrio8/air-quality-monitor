/*
 * sd_functions.c
 *
 *  Created on: 16.01.2019
 *      Author: Blazej
 */
#include "sd_functions.h"
#include "TFT.h"

void writeHeader(){
	FRESULT fresult;
	FIL file;
	fresult = f_mount(&SDFatFS, (TCHAR const*)SDPath, 1);
	fresult = open_append(&file, "AQS_data.txt");
	f_puts("AQS boot done\n", &file);
	f_puts("Time PM1 PM2.5 PM10 Formaldehyde CO2 TVOC LPG Humidity Temp\n", &file);
	fresult = f_close (&file);
	fresult = f_mount(0, "", 0);
}
void writeData(){
	RTC_TimeTypeDef current_time;
	RTC_DateTypeDef current_date;
	FRESULT fresult;
	FIL file;
	char buff[12];
	fresult = f_mount(&SDFatFS, (TCHAR const*)SDPath, 1);
	fresult = open_append(&file, "AQS_data.txt");

	HAL_RTC_GetTime(&hrtc, &current_time, RTC_FORMAT_BCD);
	HAL_RTC_GetDate(&hrtc, &current_date, RTC_FORMAT_BCD);

	//print time
	sprintf(buff, "%d", current_time.Hours);
	f_puts(buff, &file);
	f_puts(":", &file);
	sprintf(buff, "%d", current_time.Minutes);
	f_puts(buff, &file);
	f_puts(":", &file);
	sprintf(buff, "%d", current_time.Seconds);
	f_puts(buff, &file);
	f_puts(" ", &file);
    //print data
	sprintf(buff, "%d", (int)AllValues[PM1_0][0]);
	f_puts(buff, &file);
	f_puts(" ", &file);

	sprintf(buff, "%d", (int)AllValues[PM2_5][0]);
	f_puts(buff, &file);
	f_puts(" ", &file);

	sprintf(buff, "%d", (int)AllValues[PM10][0]);
	f_puts(buff, &file);
	f_puts(" ", &file);

	sprintf(buff, "%d", (int)AllValues[HCHO][0]);
	f_puts(buff, &file);
	f_puts(" ", &file);

	sprintf(buff, "%d", (int)AllValues[CO2][0]);
	f_puts(buff, &file);
	f_puts(" ", &file);

	sprintf(buff, "%d", (int)AllValues[VOC][0]);
	f_puts(buff, &file);
	f_puts(" ", &file);

	sprintf(buff, "%d", (int)AllValues[LPG][0]);
	f_puts(buff, &file);
	f_puts(" ", &file);

	sprintf(buff, "%d", (int)AllValues[HUM][0]);
	f_puts(buff, &file);
	f_puts(" ", &file);

	sprintf(buff, "%d", (int)AllValues[TEMP][0]);
	f_puts(buff, &file);
	f_puts(" ", &file);


	f_puts("\n", &file);

	fresult = f_close (&file);
	fresult = f_mount(0, "", 0);
}




FRESULT open_append (
		FIL* fp,            /* [OUT] File object to create */
		const char* path    /* [IN]  File name to be opened */
)
{
	FRESULT fr;

	/* Opens an existing file. If not exist, creates a new file. */
	fr = f_open(fp, path, FA_WRITE | FA_OPEN_ALWAYS);
	if (fr == FR_OK) {
		/* Seek to end of the file to append data */
		fr = f_lseek(fp, f_size(fp));
		if (fr != FR_OK)
			f_close(fp);
	}
	return fr;
}

