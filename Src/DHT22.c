#include "stm32f4xx.h"
#include "dwt_stm32_delay.h"
#include "DHT22.h"

void DHT_set_configuration(DHT22* sensor, uint16_t DHT_pin,
		GPIO_TypeDef* DHT_port) {
	sensor->DHT22_pin = DHT_pin;
	sensor->DHT22_port = DHT_port;

	//__HAL_RCC_GPIOD_CLK_ENABLE()
	;

}

void DHT_set_gpio_output(DHT22* sensor) {

	GPIO_InitTypeDef GPIO_Init;
	GPIO_Init.Pin |= sensor->DHT22_pin;
	GPIO_Init.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_Init.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_Init.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(sensor->DHT22_port, &GPIO_Init);
}

void DHT_set_gpio_input(DHT22* sensor) {
	GPIO_InitTypeDef GPIO_Init;
	GPIO_Init.Pin |= sensor->DHT22_pin;
	GPIO_Init.Mode = GPIO_MODE_INPUT;
	GPIO_Init.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(sensor->DHT22_port, &GPIO_Init);
}

void DHT_start(DHT22* sensor) {

	DWT_Init();
	DHT_set_gpio_output(sensor);  // set the pin as output
	HAL_GPIO_WritePin(sensor->DHT22_port, sensor->DHT22_pin, 0); // pull the pin low
	DWT_Delay(1000);   // wait for 1000us
	DHT_set_gpio_input(sensor);   // set as input
	// wait for the pin to go low
	//uwTick = 0;
	int tick = HAL_GetTick();
	while ((HAL_GPIO_ReadPin(sensor->DHT22_port, sensor->DHT22_pin)))
	{
		if(HAL_GetTick()-tick > 100)
			return ;
	}
}

uint8_t DHT_check_response(DHT22* sensor) {
	uint8_t check = 0;
	if (!(HAL_GPIO_ReadPin(sensor->DHT22_port, sensor->DHT22_pin))) {
		DWT_Delay(100);
		if ((HAL_GPIO_ReadPin(sensor->DHT22_port, sensor->DHT22_pin)))
			check = 1;
	}
	 // wait for the pin to go low
	//uwTick = 0;
	int tick = HAL_GetTick();
	while ((HAL_GPIO_ReadPin(sensor->DHT22_port, sensor->DHT22_pin)))
	{
		if(HAL_GetTick()-tick > 100)
			return check;
	}

	return check;
}

uint8_t DHT_read_byte(DHT22* sensor) {
	uint8_t i, j;
	for (j = 0; j < 8; j++) {
		while (!(HAL_GPIO_ReadPin(sensor->DHT22_port, sensor->DHT22_pin)))
			;   // wait for the pin to go high
		DWT_Delay(40);   // wait for 40 us
		if ((HAL_GPIO_ReadPin(sensor->DHT22_port, sensor->DHT22_pin)) == 0) // if the pin is low
				{
			i &= ~(1 << (7 - j));   // write 0
		} else
			i |= (1 << (7 - j));  // if the pin is high, write 1
		//uwTick = 0;
		int tick = HAL_GetTick();
		//wait for pin to go low
		while ((HAL_GPIO_ReadPin(sensor->DHT22_port, sensor->DHT22_pin)))
		{
			if(HAL_GetTick()-tick > 100)
				return 0 ;
		}

	}
	return i;
}

int DHT_read_data(DHT22* sensor) {
	uint8_t Rh_byte1, Rh_byte2, Temp_byte1, Temp_byte2;
	int16_t Temp, Hum;

	Rh_byte1 = DHT_read_byte(sensor);
	Rh_byte2 = DHT_read_byte(sensor);
	Temp_byte1 = DHT_read_byte(sensor);
	Temp_byte2 = DHT_read_byte(sensor);
	sensor->checksum = DHT_read_byte(sensor);
	Temp = (Temp_byte1 << 8) | Temp_byte2;
	Hum = (Rh_byte1 << 8) | Rh_byte2;
	sensor->Temperature = (float) Temp / 10;
	sensor->Humidity = (float) Hum / 10;
	//After all data are received control checksum should be checked.
	uint8_t sum = 0;
	sum = Rh_byte1+Rh_byte2+Temp_byte1+Temp_byte2;

	if(sum == sensor->checksum)
	{
		return 1;
	}
	else
		return 0;
}

float DHT_get_temperature(DHT22* sensor) {
	return sensor->Temperature;
}

float DHT_get_humidity(DHT22* sensor) {
	return sensor->Humidity;
}


