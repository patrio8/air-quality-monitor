
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "fatfs.h"
#include "usb_device.h"

/* USER CODE BEGIN Includes */

#include "Adafruit_HX8357.h"
#include "TFT.h"
#include "PMS5003ST.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;
ADC_HandleTypeDef hadc3;

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;

RTC_HandleTypeDef hrtc;

SD_HandleTypeDef hsd;

SPI_HandleTypeDef hspi2;
SPI_HandleTypeDef hspi4;

TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart8;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
//PMS sensor variables

//Struktury czujnika PMS
PMS5003ST_config config;
PMS5003ST PMS;
//flaga informuj�ca o tym, �e dane z PMS5003ST s� dost�pne
volatile int pms_data_ready = 0;

//Warto�c pobrana z czujnika LPG
volatile int MQval = 0 ;
//Zmienna przechowuj�ca infomacj� o tym jaki aktualnie ekran jest wy�wietlany
CurrentScreen screen;

//Licznik godzin - do zapisu wartosci historycznych
int hourcnt;

//Obszary wy�wietlane na wszystkich ekranach szczeg�owych
CircleArea DetailedScreens[MAX_NUM_OF_MESUREMENTS][CIRC_SCR_MAX_AREAS];

//Normy : PM10, PM2.5, CO2, HCHO
float  norms[MAX_NUM_OF_MESUREMENTS];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_ADC2_Init(void);
static void MX_ADC3_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2C2_Init(void);
static void MX_SDIO_SD_Init(void);
static void MX_SPI2_Init(void);
static void MX_SPI4_Init(void);
static void MX_TIM2_Init(void);
static void MX_UART8_Init(void);
static void MX_RTC_Init(void);

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
                                

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc);
void SaveLastVal();
void CalcDailyAverage();
void ExcStandards();
void InitAllValues();
void InitLast24values();

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  //Inicjalizacja zmiennych aktywuj�cych poszczeg�lne funkcji w p�tli while:
  //licznik sekund (maksymalna warto�c to 10)
  //wyswietlanie daty i godziny (co 10s)
  //przekopiowanie danych z czujnik�w do zbiorczej tablicy
  //od�wie�anie ekranu (co 2s)
  //zapis danych archiwalnych (co 1h)

  sec_counter = 0;
  time_disp_enbl = 0;
  save_val_enbl = 0;
  scr_refresh_enbl = 0;
  last_val_save_enable = 0;

  //Inicjalizacja czujnika PMS
  PMS_set_configuration(&config,&huart8,SET_PMS5003ST_Pin,SET_PMS5003ST_GPIO_Port,
  		RESET_PMS5003ST_Pin,RESET_PMS5003ST_GPIO_Port);
  PMS_initialize(&config,&PMS);

//Inicjalizacja czujnika DHT22

  DHT_set_configuration(&DHT_sensor,DHT22_Pin,DHT22_GPIO_Port);
//Wprowadzenie warto�ci norm
	norms[PM10] = 50;
	norms[PM2_5] = 25;
	norms[CO2] = 1000;
	norms[HCHO] = 0.5;
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_ADC2_Init();
  MX_ADC3_Init();
  MX_I2C1_Init();
  MX_I2C2_Init();
  MX_SDIO_SD_Init();
  MX_SPI2_Init();
  MX_SPI4_Init();
  MX_TIM2_Init();
  MX_UART8_Init();
  MX_USB_DEVICE_Init();
  MX_FATFS_Init();
  MX_RTC_Init();
  /* USER CODE BEGIN 2 */

  //W��czenie czujnika PMS
  PMS_sleepOFF(&PMS);
  HAL_GPIO_WritePin(RESET_PMS5003ST_GPIO_Port,RESET_PMS5003ST_Pin,GPIO_PIN_SET);
  HAL_UART_Receive_IT(&huart8,PMS.PMS_DATA,40);

  //Inicjalizacja ekranu
  begin(HX8357D);
  fillScreen(HX8357_WHITE);
  setRotation(3);


  //MainScrArea = [ PM1_0, PM2_5, PM10, CO2, HCHO, TEMP, HUM, SETTINGS]
  MainScrArea MainScr1[MAIN_SCR_MAX_AREAS];
  //SecondScrArea = [part03,part05,part1,part2.5,part5,part10,LPG]
  MainScrArea MainScr2[MAIN_SCR_MAX_AREAS];

  //Inicjalizacja ekran�w g��wnych oraz szczeg�owych
  InitMainScreen(MainScr1);
  InitSecondScreen(MainScr2);
  InitDetailedScreen(DetailedScreens);

  //Ustawienie prog�w ostrzegawczych i alarmowych dla pomair�w na 1 ekranie g��wnym
  SetMainScrAreaThresholds(&MainScr1[0],50,75);
  SetMainScrAreaThresholds(&MainScr1[1],100,150);
  SetMainScrAreaThresholds(&MainScr1[2],200,300);
  SetMainScrAreaThresholds(&MainScr1[3],2000,4000);
  SetMainScrAreaThresholds(&MainScr1[4],1,2);
  SetMainScrAreaThresholds(&MainScr1[5],27,50);
  SetMainScrAreaThresholds(&MainScr1[6],65,95);

  //Struktura przechowuj�ca informacje o wykresie,
  //inicjalizowana odpowiednimi danymi przed wy�wietleniem
  Chart chart;


  HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);

//Inicjalizacja tablic przechwuj�cych dane pomiarowe
  InitLast24values();
  InitAllValues();

  //Ustawienie pocz�tkowej daty
  	  RTC_TimeTypeDef current_time;
      current_time.Hours = 9;
      current_time.Minutes = 59;
      current_time.Seconds = 0;
      RTC_DateTypeDef current_date;
      current_date.Year = 19;
      current_date.Month = RTC_MONTH_JANUARY;
      current_date.Date = 20;
      HAL_RTC_SetTime(&hrtc, &current_time, RTC_FORMAT_BCD);
      HAL_RTC_SetDate(&hrtc,&current_date,RTC_FORMAT_BCD);

      //Inicjalizacja czujnika VOC
      initTVOC();

    hourcnt = 23;
    //Wy�wietlenie 1 ekranu g�wnego
    screen = DisplayMainScreen(MainScr1);
  	HAL_ADC_Start_IT(&hadc3);
  	//Tablice do przechowywania aktualej daty i godziny
 	char time[9];
 	char date[11];


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

	  //Wypisywanie godziny i daty co 10s
if(time_disp_enbl == 1)
{
	HAL_RTC_GetTime(&hrtc, &current_time, RTC_FORMAT_BCD);
	HAL_RTC_GetDate(&hrtc, &current_date, RTC_FORMAT_BCD);
	  sprintf(time, "%02u:%02u", current_time.Hours,current_time.Minutes);
	  drawString(time,410,1,2,WHITE);
	  sprintf(date, "%02u-%02u-%04u",current_date.Date ,current_date.Month,current_date.Year+2000);
	  drawString(date,395,15,2,WHITE);
	  time_disp_enbl = 0;
}

	//Od�wie�anie ekran�w oraz obs�uga dotyku
	  switch(screen)
	 	 {
	 	 case MainScr:
	 		 if(scr_refresh_enbl == 1)
	 		 {screen = RefreshMainScreen(screen,MainScr1,AllValues,MainScr2,DetailedScreens);scr_refresh_enbl = 0;}
	 		 if(screen == MainScr)
	 		 screen = MainScrTouchDetect(MainScr,DetailedScreens,MainScr2);
	 		 break;
	 	 case SecondScr:
	 		if(scr_refresh_enbl == 1)
	 		 {screen = RefreshMainScreen(screen,MainScr2,AllValues,MainScr1,DetailedScreens);scr_refresh_enbl = 0;}
	 		 if(screen == SecondScr)
	 		 screen = SecondScrTouchDetect(SecondScr,DetailedScreens,MainScr1);
	 		 break;
	 	 case LineChartScr:
	 		 screen = ChartScrTouchDetect(&chart,screen,DetailedScreens);
	 		 break;
	 	 case BarChartScr:
	 		 screen = ChartScrTouchDetect(&chart,screen,DetailedScreens);
	 		 break;
	 	 case PM1_0DetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(PM1_0,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr1,last24val,current_time.Hours);
	 		 break;
	 	 case PM2_5DetailScr:
	 		if(scr_refresh_enbl == 1)
	 		{ RefreshDetailScreen(PM2_5,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr1,last24val,current_time.Hours);
	 		 break;
	 	 case PM10DetailScr:
	 		if(scr_refresh_enbl == 1)
	 		{RefreshDetailScreen(PM10,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr1,last24val,current_time.Hours);
	 		 break;
	 	 case TempDetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(TEMP,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr1,last24val,current_time.Hours);
	 		 break;
	 	 case HumDetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(HUM,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr1,last24val,current_time.Hours);
	 		 break;
	 	 case CO2DetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(CO2,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr1,last24val,current_time.Hours);
	 		 break;
	 	 case HCHODetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(HCHO,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr1,last24val,current_time.Hours);
	 		 break;
	 	 case VOCDetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(VOC,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr1,last24val,current_time.Hours);
	 		 break;
	 	 case PART_03DetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(PART_03,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr2,last24val,current_time.Hours);
	 		 break;
	 	 case PART_05DetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(PART_05,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr2,last24val,current_time.Hours);
	 		 break;
	 	 case PART_1DetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(PART_1,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr2,last24val,current_time.Hours);
	 		 break;
	 	 case PART_2_5DetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(PART_2_5,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr2,last24val,current_time.Hours);
	 		 break;
	 	 case PART_5DetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(PART_5,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr2,last24val,current_time.Hours);
	 		 break;
	 	 case PART_10DetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(PART_10,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr2,last24val,current_time.Hours);
	 		 break;
	 	 case LPGDetailScr:
	 		if(scr_refresh_enbl == 1)
	 		 {RefreshDetailScreen(LPG,DetailedScreens,AllValues);scr_refresh_enbl = 0;}
	 		 screen = DetailScrTouchDetect(screen,&chart,MainScr2,last24val,current_time.Hours);
	 		 break;
	 	 case SettingsScr:
	 		screen = SettingsScrTouchDetect(MainScr2,&time_to_set,&date_to_set);
	 		break;
	 	 default:
	 		 //
	 		 break;
	 	 }


	 	 //Przeliczenie  danych pobranych z sensora PMS5003ST
	 	  if(pms_data_ready)
	 	  {
	 		  PMS_get_data(&PMS);
	 		  pms_data_ready = 0;
	 	  }

		if(save_val_enbl == 1)
		{
			//Je�li dane z czujnika DHT22 s� poprawne to zapisz
			if(crc_correct == 1)
			{
			 	  if(DHT_get_temperature(&DHT_sensor) != 0)
				  {
			 		  AllValues[TEMP][0] = DHT_get_temperature(&DHT_sensor);
			 		  AllValues[HUM][0] = DHT_get_humidity(&DHT_sensor);
				  }
			}

			//Zapis aktualych warto�ci pomiar�w do tablicy
			AllValues[PM1_0][0] = (float)PMS_get_PM1_0(&PMS);
			AllValues[PM2_5][0] = (float)PMS_get_PM2_5(&PMS);
			AllValues[PM10][0] = (float)PMS_get_PM10(&PMS);
			AllValues[TEMP][0] = (AllValues[TEMP][0]+ PMS_get_temp(&PMS))/2;
			AllValues[HUM][0] = (AllValues[HUM][0] + PMS_get_hum(&PMS))/2;
			AllValues[HCHO][0] = PMS_get_form(&PMS);
			AllValues[PART_03][0] = PMS_get_part_0_3(&PMS);
			AllValues[PART_05][0] = PMS_get_part_0_5(&PMS);
			AllValues[PART_1][0] = PMS_get_part_1_0(&PMS);
			AllValues[PART_2_5][0] = PMS_get_part_2_5(&PMS);
			AllValues[PART_5][0] = PMS_get_part_5_0(&PMS);
			AllValues[PART_10][0] = PMS_get_part_10(&PMS);
			AllValues[CO2][0] = readCO2();
			AllValues[VOC][0] = TVOC;
			AllValues[LPG][0] = (MQval/4095)*100;

	 	  save_val_enbl = 0;

		}

			//Funkcje wywo�ywane o ka�dej pe�nej godzinie:
			//Nast�puje zapis ostatnich pomiar�w, policzenie waro�ci �redniodobowych oraz,
			//ewentualne przekroczenie normy
	 	  	if(last_val_save_enable == 1)
	 	  	{
	 	  		SaveLastVal();
	 	  		CalcDailyAverage();
	 	  		ExcStandards();

	 	 	  last_val_save_enable = 0;
	 		}
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 12;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_10;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* ADC2 init function */
static void MX_ADC2_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
    */
  hadc2.Instance = ADC2;
  hadc2.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc2.Init.Resolution = ADC_RESOLUTION_12B;
  hadc2.Init.ScanConvMode = DISABLE;
  hadc2.Init.ContinuousConvMode = DISABLE;
  hadc2.Init.DiscontinuousConvMode = DISABLE;
  hadc2.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc2.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc2.Init.NbrOfConversion = 1;
  hadc2.Init.DMAContinuousRequests = DISABLE;
  hadc2.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* ADC3 init function */
static void MX_ADC3_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
    */
  hadc3.Instance = ADC3;
  hadc3.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc3.Init.Resolution = ADC_RESOLUTION_12B;
  hadc3.Init.ScanConvMode = DISABLE;
  hadc3.Init.ContinuousConvMode = ENABLE;
  hadc3.Init.DiscontinuousConvMode = DISABLE;
  hadc3.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc3.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc3.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc3.Init.NbrOfConversion = 1;
  hadc3.Init.DMAContinuousRequests = DISABLE;
  hadc3.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_13;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc3, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* I2C2 init function */
static void MX_I2C2_Init(void)
{

  hi2c2.Instance = I2C2;
  hi2c2.Init.ClockSpeed = 100000;
  hi2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* RTC init function */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime;
  RTC_DateTypeDef sDate;
  RTC_AlarmTypeDef sAlarm;

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */

    /**Initialize RTC Only 
    */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initialize RTC and set the Time and Date 
    */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Enable the Alarm A 
    */
  sAlarm.AlarmTime.Hours = 0x0;
  sAlarm.AlarmTime.Minutes = 0x0;
  sAlarm.AlarmTime.Seconds = 0x0;
  sAlarm.AlarmTime.SubSeconds = 0x0;
  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  sAlarm.AlarmMask = RTC_ALARMMASK_ALL;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  sAlarm.AlarmDateWeekDay = 0x1;
  sAlarm.Alarm = RTC_ALARM_A;
  if (HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* SDIO init function */
static void MX_SDIO_SD_Init(void)
{

  hsd.Instance = SDIO;
  hsd.Init.ClockEdge = SDIO_CLOCK_EDGE_RISING;
  hsd.Init.ClockBypass = SDIO_CLOCK_BYPASS_DISABLE;
  hsd.Init.ClockPowerSave = SDIO_CLOCK_POWER_SAVE_DISABLE;
  hsd.Init.BusWide = SDIO_BUS_WIDE_1B;
  hsd.Init.HardwareFlowControl = SDIO_HARDWARE_FLOW_CONTROL_DISABLE;
  hsd.Init.ClockDiv = 255;

}

/* SPI2 init function */
static void MX_SPI2_Init(void)
{

  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_HARD_INPUT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* SPI4 init function */
static void MX_SPI4_Init(void)
{

  /* SPI4 parameter configuration*/
  hspi4.Instance = SPI4;
  hspi4.Init.Mode = SPI_MODE_MASTER;
  hspi4.Init.Direction = SPI_DIRECTION_2LINES;
  hspi4.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi4.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi4.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi4.Init.NSS = SPI_NSS_SOFT;
  hspi4.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi4.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi4.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi4.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi4.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 0;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim2);

}

/* UART8 init function */
static void MX_UART8_Init(void)
{

  huart8.Instance = UART8;
  huart8.Init.BaudRate = 9600;
  huart8.Init.WordLength = UART_WORDLENGTH_8B;
  huart8.Init.StopBits = UART_STOPBITS_1;
  huart8.Init.Parity = UART_PARITY_NONE;
  huart8.Init.Mode = UART_MODE_TX_RX;
  huart8.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart8.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_MultiProcessor_Init(&huart8, 0, UART_WAKEUPMETHOD_IDLELINE) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
     PB0   ------> S_TIM3_CH3
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, LCD_RST_Pin|LCD_CS_Pin|nRESET_CCS811_Pin|nWAKE_CCS811_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LCD_DC_GPIO_Port, LCD_DC_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, NRF_CE_Pin|LED1_Pin|LED2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, RESET_PMS5003ST_Pin|SET_PMS5003ST_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : LCD_RST_Pin LCD_CS_Pin nRESET_CCS811_Pin nWAKE_CCS811_Pin */
  GPIO_InitStruct.Pin = LCD_RST_Pin|LCD_CS_Pin|nRESET_CCS811_Pin|nWAKE_CCS811_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : MQ5_DOUT_Pin */
  GPIO_InitStruct.Pin = MQ5_DOUT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(MQ5_DOUT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LCD_DC_Pin */
  GPIO_InitStruct.Pin = LCD_DC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LCD_DC_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LCD_backlight_Pin */
  GPIO_InitStruct.Pin = LCD_backlight_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
  HAL_GPIO_Init(LCD_backlight_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : NRF_IRQ_Pin DHT22_Pin */
  GPIO_InitStruct.Pin = NRF_IRQ_Pin|DHT22_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : NRF_CE_Pin LED1_Pin LED2_Pin */
  GPIO_InitStruct.Pin = NRF_CE_Pin|LED1_Pin|LED2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : fotorezystor_Pin */
  GPIO_InitStruct.Pin = fotorezystor_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(fotorezystor_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RESET_PMS5003ST_Pin SET_PMS5003ST_Pin */
  GPIO_InitStruct.Pin = RESET_PMS5003ST_Pin|SET_PMS5003ST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	pms_data_ready = 1;
	//PMS_get_data(&PMS);
	HAL_UART_Receive_IT(&huart8, PMS.PMS_DATA,40);
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	MQval = HAL_ADC_GetValue(&hadc3); // Pobranie zmierzonej wartosci
}

void SaveLastVal()
{
	int i;
			for(i = 0; i<MAX_NUM_OF_MESUREMENTS; i++)
			{
				last24val[i][hourcnt] = AllValues[i][0];
			}
			hourcnt -= 1;
			if(hourcnt < 0)
				hourcnt = 23;
}


void CalcDailyAverage()
{
		//Average value
	int i,j,sum;
	 	  for(i = 0 ; i<MAX_NUM_OF_MESUREMENTS; i++)
	 	  {
	 		  sum = 0;
	 		  for(j = 0 ; j <24 ; j++)
	 		  {
	 			 sum += last24val[i][j];
	 		  }
	 		  AllValues[i][1] = (float)sum/24;
	 	  }
}

void ExcStandards()
{
	  // Normy
	  //Przekroczenie normy PM10
	  int diff;
	  diff = AllValues[PM10][1]-norms[PM10];
	  if(diff > 0)
		AllValues[PM10][2]= (diff/norms[PM10])*100;
	  else
		  AllValues[PM10][2] = 0;
	  //Przekroczenie normy PM2.5
	  diff = AllValues[PM2_5][1]-norms[PM2_5];
	  if(diff > 0)
		AllValues[PM2_5][2]= (diff/norms[PM2_5])*100;
	  else
		  AllValues[PM2_5][2] = 0;
	  //Przekroczenie normy CO2
	  diff = AllValues[CO2][1]-norms[CO2];
	  if(diff > 0)
		AllValues[CO2][2]= (diff/norms[CO2])*100;
	  else
		  AllValues[CO2][2] = 0;
	  //Przekroczenie normy Formaldehydu
	  diff = AllValues[HCHO][1]-norms[HCHO];
	  if(diff > 0)
		AllValues[PM10][2]= (diff/norms[HCHO])*100;
	  else
		  AllValues[HCHO][2] = 0;

}

void InitLast24values()
{
	  int n,m;
	  for(n= 0 ; n< MAX_NUM_OF_MESUREMENTS; n++)
	  {
	  	for(m = 0; m <24;m++)
	  	{
	  		last24val[n][m] = 0;
	  	}
	  }
}

void InitAllValues()
{
	int n,m;
	for(n= 0 ; n< MAX_NUM_OF_MESUREMENTS; n++)
	  {
	  	for(m = 0; m <3;m++)
	  	{
	  		AllValues[n][m] = -1;
	  	}
	  }
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
