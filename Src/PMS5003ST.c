#include "PMS5003ST.h"

void PMS_set_configuration(PMS5003ST_config* config, UART_HandleTypeDef* huart, uint16_t set_pin, GPIO_TypeDef* set_port, uint16_t reset_pin, GPIO_TypeDef* reset_port)
 {
     config->PMS_huart = huart;
     config->PMS_SET_pin = set_pin;
     config->PMS_SET_port = set_port;
     config->PMS_RESET_pin = set_pin;
     config->PMS_RESET_port = reset_port;
 }

 void PMS_initialize(PMS5003ST_config *config, PMS5003ST* sensor)
 {
     sensor->configuration = *config;
 }

 void PMS_read(PMS5003ST* sensor)
 {
	 HAL_UART_Receive_IT(sensor->configuration.PMS_huart,sensor->PMS_DATA,BUFFER_SIZE);
 }

 void PMS_sleepON(PMS5003ST* sensor)
 {
	 HAL_GPIO_WritePin(sensor->configuration.PMS_SET_port, sensor->configuration.PMS_SET_pin, GPIO_PIN_RESET);
 }

 void PMS_sleepOFF(PMS5003ST* sensor)
 {
	 HAL_GPIO_WritePin(sensor->configuration.PMS_SET_port, sensor->configuration.PMS_SET_pin, GPIO_PIN_SET);
 }

 int PMS_get_PM1_0(PMS5003ST* sensor)
 {
	 return sensor->PM1_0;
 }

 int PMS_get_PM2_5(PMS5003ST* sensor)
 {
	 return sensor->PM2_5;
 }

 int PMS_get_PM10(PMS5003ST* sensor)
 {
	 return sensor->PM10;
 }

 int PMS_get_PM1_0env(PMS5003ST* sensor)
 {
	 return sensor->PM1_0env;
 }

 int PMS_get_PM2_5env(PMS5003ST* sensor)
 {
	 return sensor->PM2_5env;
 }

 int PMS_get_PM10env(PMS5003ST* sensor)
 {
	 return sensor->PM10env;
 }

 int PMS_get_part_0_3(PMS5003ST* sensor)
 {
	 return sensor->particles_0_3;
 }

 int PMS_get_part_0_5(PMS5003ST* sensor)
 {
	 return sensor->particles_0_5;
 }

 int PMS_get_part_1_0(PMS5003ST* sensor)
 {
	 return sensor->particles_1_0;
 }

 int PMS_get_part_2_5(PMS5003ST* sensor)
 {
	 return sensor->particles_2_5;
 }

 int PMS_get_part_5_0(PMS5003ST* sensor)
 {
	 return sensor->particles_5_0;
 }

 int PMS_get_part_10(PMS5003ST* sensor)
 {
	 return sensor->particles_10;
 }

 float PMS_get_form(PMS5003ST* sensor)
 {
	 return sensor->formaldehyde;
 }

 float PMS_get_temp(PMS5003ST* sensor)
 {
	 return sensor->temperature;
 }

 float PMS_get_hum(PMS5003ST* sensor)
 {
	 return sensor->humidity;
 }

 void PMS_get_data(PMS5003ST* sensor)
 {
		sensor->frame_length =sensor->PMS_DATA[3]; //(sensor->PMS_DATA[2])<<8 + sensor->PMS_DATA[3];

	 	sensor->PM1_0 = (sensor->PMS_DATA[4]<<8) + sensor->PMS_DATA[5];
		sensor->PM2_5 = (sensor->PMS_DATA[6]<<8) + sensor->PMS_DATA[7];
		sensor->PM10 = (sensor->PMS_DATA[8]<<8) + sensor->PMS_DATA[9];

	 	sensor->PM1_0env = (sensor->PMS_DATA[10]<<8) + sensor->PMS_DATA[11];
		sensor->PM2_5env = (sensor->PMS_DATA[12]<<8) + sensor->PMS_DATA[13];
		sensor->PM10env = (sensor->PMS_DATA[14]<<8) + sensor->PMS_DATA[15];

	 	sensor->particles_0_3 = (sensor->PMS_DATA[16]<<8) + sensor->PMS_DATA[17];
		sensor->particles_0_5 = (sensor->PMS_DATA[18]<<8) + sensor->PMS_DATA[19];
		sensor->particles_1_0 = (sensor->PMS_DATA[20]<<8) + sensor->PMS_DATA[21];
		sensor->particles_2_5 = (sensor->PMS_DATA[22]<<8) + sensor->PMS_DATA[23];
		sensor->particles_5_0 = (sensor->PMS_DATA[24]<<8) + sensor->PMS_DATA[25];
		sensor->particles_10 = (sensor->PMS_DATA[26]<<8) + sensor->PMS_DATA[27];

		sensor->formaldehyde = (float)((sensor->PMS_DATA[28]<<8) + sensor->PMS_DATA[29])/1000;
		sensor->temperature = (float)((sensor->PMS_DATA[30]<<8) + sensor->PMS_DATA[31])/10;
		sensor->humidity = (float)((sensor->PMS_DATA[32]<<8) + sensor->PMS_DATA[33])/10;

 }

 void PMS_set_active_mode(PMS5003ST* sensor)
 {
	 uint8_t command[] = {0x42,0x4D,0xE1,0x00,0x01,0x01,0x71};

	 HAL_UART_Transmit(sensor->configuration.PMS_huart,command,7,1);
 }

 void PMS_set_passive_mode(PMS5003ST* sensor)
 {
	 uint8_t command[] = {0x42,0x4D,0xE1,0x00,0x00,0x01,0x70};

	 HAL_UART_Transmit(sensor->configuration.PMS_huart,command,7,1);

 }
