/*
 * sensors_I2C.c
 *
 *  Created on: 16.01.2019
 *      Author: Blazej
 */
#include "sensors_I2C.h"

uint8_t readBatteryValue(){
	uint8_t data_to_send[3];
	uint8_t reg = 0;
	uint8_t battery_voltage = 0x00;
	uint8_t system_voltage = 0x00;
	uint8_t usb_voltage = 0x00;
	uint8_t discharge_current =0;
	uint8_t charge_current=0;
	uint16_t max_charge_voltage=0;

	reg = 0x01;
	data_to_send[0]=reg;
	data_to_send[1]=0x00;
	HAL_I2C_Master_Transmit(&hi2c1, 0xD6, data_to_send, 2, 1000);

	reg = 0x3B;
	data_to_send[0]=reg;
	data_to_send[1]=0xE0; //E0 continous
	HAL_I2C_Master_Transmit(&hi2c1, 0xD6, data_to_send, 2, 1000);

	reg = 0x3A;
	data_to_send[0]=reg;
	data_to_send[1]=0xFF;
	HAL_I2C_Master_Transmit(&hi2c1, 0xD6, data_to_send, 2, 1000);


	reg = 0x2C;
	data_to_send[0]=reg;
	HAL_I2C_Master_Transmit(&hi2c1, 0xD6, data_to_send, 1, 1000);
	HAL_I2C_Master_Receive(&hi2c1, 0xD7, &battery_voltage, 1, 1000);

	reg = 0x2D;
	data_to_send[0]=reg;
	HAL_I2C_Master_Transmit(&hi2c1, 0xD6, data_to_send, 1, 1000);
	HAL_I2C_Master_Receive(&hi2c1, 0xD7, &system_voltage, 1, 1000);

	reg = 0x27;
	data_to_send[0]=reg;
	HAL_I2C_Master_Transmit(&hi2c1, 0xD6, data_to_send, 1, 1000);
	HAL_I2C_Master_Receive(&hi2c1, 0xD7, &usb_voltage, 1, 1000);

	reg = 0x29;
	data_to_send[0]=reg;
	HAL_I2C_Master_Transmit(&hi2c1, 0xD6, data_to_send, 1, 1000);
	HAL_I2C_Master_Receive(&hi2c1, 0xD7, &charge_current, 1, 1000);

	reg = 0x28;
	data_to_send[0]=reg;
	HAL_I2C_Master_Transmit(&hi2c1, 0xD6, data_to_send, 1, 1000);
	HAL_I2C_Master_Receive(&hi2c1, 0xD7, &discharge_current, 1, 1000);

	reg = 0x04;
	data_to_send[0]=reg;
	HAL_I2C_Master_Transmit(&hi2c1, 0xD6, data_to_send, 1, 1000);
	HAL_I2C_Master_Receive(&hi2c1, 0xD7, &max_charge_voltage, 2, 1000);

	return battery_voltage;

}


int readCO2(){
	uint8_t data_to_send[5];
	uint8_t reg = 0;
	uint8_t recieved_data[4];
	int ppmValue;
	HAL_StatusTypeDef done = HAL_OK;

	reg = 0x04;
	data_to_send[0]=reg;
	data_to_send[1]=0x13;
	data_to_send[2]=0x8B;
	data_to_send[3]=0x00;
	data_to_send[4]=0x01;
	done = HAL_I2C_Master_Transmit(&hi2c1, 0x2A, data_to_send, 5, 1000);
	done = HAL_I2C_Master_Receive(&hi2c1, 0x2B, recieved_data, 4, 1000);

	ppmValue = (((recieved_data[2] & 0x3F ) << 8) | recieved_data[3]);

	return ppmValue;
}

void readTVOC(int *tvoc, int *co2eq){
	uint8_t data_to_send[5];
	uint8_t reg = 0;
	uint8_t recieved_data[6];
	HAL_StatusTypeDef done = HAL_OK;

	data_to_send[1]=0x08;
	data_to_send[0]=0x20;
	done = HAL_I2C_Master_Transmit(&hi2c1, 0xB0, data_to_send, 2, 1000);
	HAL_Delay(10);
	done = HAL_I2C_Master_Receive(&hi2c1, 0xB1, recieved_data, 6, 1000);

	*co2eq = recieved_data[0]<<8 | recieved_data[1];
	*tvoc = recieved_data[3]<<8 | recieved_data[4];

}
void initTVOC(){
	uint8_t data_to_send[2];
	HAL_StatusTypeDef done = HAL_OK;
	data_to_send[1]=0x03;
	data_to_send[0]=0x20;
	done = HAL_I2C_Master_Transmit(&hi2c1, 0xB0, data_to_send, 2, 1000);
	HAL_Delay(10);
}
