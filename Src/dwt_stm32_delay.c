/*
 * dwt_stm32_delay.c
 *
 *  Created on: 11.11.2018
 *      Author: Patryk Forencewicz
 */

#include "dwt_stm32_delay.h"
#include "stm32f4xx.h"

    /**
     * @brief Initializes DWT_Clock_Cycle_Count for DWT_Delay_us function
     * @return Error DWT counter
     * 1: clock cycle counter not started
     * 0: clock cycle counter works
     */
    void DWT_Init(void)
{
    if (!(CoreDebug->DEMCR & CoreDebug_DEMCR_TRCENA_Msk)) {
        CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
        DWT->CYCCNT = 0;
        DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
    }
}

/**
 * Delay routine itself.
 * Time is in microseconds (1/1000000th of a second), not to be
 * confused with millisecond (1/1000th).
 *
 * @param uint32_t us  Number of microseconds to delay for
 */
void DWT_Delay(uint32_t us) // microseconds
{
  int32_t targetTick = DWT->CYCCNT + us * (SystemCoreClock/1000000);
  while (DWT->CYCCNT <= targetTick);
}
