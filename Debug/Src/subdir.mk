################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/Adafruit_GFX_AS.c \
../Src/Adafruit_HX8357.c \
../Src/DHT22.c \
../Src/Font16.c \
../Src/Font32.c \
../Src/Font64.c \
../Src/Font7s.c \
../Src/PMS5003ST.c \
../Src/TFT.c \
../Src/TouchScreen.c \
../Src/bsp_driver_sd.c \
../Src/dwt_stm32_delay.c \
../Src/fatfs.c \
../Src/glcdfont.c \
../Src/main.c \
../Src/sd_diskio.c \
../Src/sd_functions.c \
../Src/sensors_I2C.c \
../Src/stm32f4xx_hal_msp.c \
../Src/stm32f4xx_it.c \
../Src/system_stm32f4xx.c \
../Src/usb_device.c \
../Src/usbd_cdc_if.c \
../Src/usbd_conf.c \
../Src/usbd_desc.c 

OBJS += \
./Src/Adafruit_GFX_AS.o \
./Src/Adafruit_HX8357.o \
./Src/DHT22.o \
./Src/Font16.o \
./Src/Font32.o \
./Src/Font64.o \
./Src/Font7s.o \
./Src/PMS5003ST.o \
./Src/TFT.o \
./Src/TouchScreen.o \
./Src/bsp_driver_sd.o \
./Src/dwt_stm32_delay.o \
./Src/fatfs.o \
./Src/glcdfont.o \
./Src/main.o \
./Src/sd_diskio.o \
./Src/sd_functions.o \
./Src/sensors_I2C.o \
./Src/stm32f4xx_hal_msp.o \
./Src/stm32f4xx_it.o \
./Src/system_stm32f4xx.o \
./Src/usb_device.o \
./Src/usbd_cdc_if.o \
./Src/usbd_conf.o \
./Src/usbd_desc.o 

C_DEPS += \
./Src/Adafruit_GFX_AS.d \
./Src/Adafruit_HX8357.d \
./Src/DHT22.d \
./Src/Font16.d \
./Src/Font32.d \
./Src/Font64.d \
./Src/Font7s.d \
./Src/PMS5003ST.d \
./Src/TFT.d \
./Src/TouchScreen.d \
./Src/bsp_driver_sd.d \
./Src/dwt_stm32_delay.d \
./Src/fatfs.d \
./Src/glcdfont.d \
./Src/main.d \
./Src/sd_diskio.d \
./Src/sd_functions.d \
./Src/sensors_I2C.d \
./Src/stm32f4xx_hal_msp.d \
./Src/stm32f4xx_it.d \
./Src/system_stm32f4xx.d \
./Src/usb_device.d \
./Src/usbd_cdc_if.d \
./Src/usbd_conf.d \
./Src/usbd_desc.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F427xx -I"E:/STM/Workspace/AQS/Inc" -I"E:/STM/Workspace/AQS/Drivers/STM32F4xx_HAL_Driver/Inc" -I"E:/STM/Workspace/AQS/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"E:/STM/Workspace/AQS/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"E:/STM/Workspace/AQS/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"E:/STM/Workspace/AQS/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"E:/STM/Workspace/AQS/Drivers/CMSIS/Include" -I"E:/STM/Workspace/AQS/Middlewares/Third_Party/FatFs/src"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


