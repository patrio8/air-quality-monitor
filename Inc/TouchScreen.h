// Touch screen library with X Y and Z (pressure) readings as well
// as oversampling to avoid 'bouncing'
// (c) ladyada / adafruit
// Code under MIT License

#ifndef _ADAFRUIT_TOUCHSCREEN_H_
#define _ADAFRUIT_TOUCHSCREEN_H_
#include <stdint.h>
#include "main.h"

#include "stdbool.h"
#if defined (__AVR__) || defined(TEENSYDUINO)
  #include <avr/pgmspace.h>
#elif defined(ESP8266)
  #include <pgmspace.h>
#endif

// define here the size of a register!

#if defined (__arm__) || defined(ARDUINO_STM32_FEATHER)
 #if defined(TEENSYDUINO)
  typedef volatile uint8_t RwReg;
  #define USE_FAST_PINIO
 #elif defined(NRF52) || defined(ARDUINO_MAXIM)
  typedef volatile uint32_t RwReg;
  //#define USE_FAST_PINIO
 #elif defined(ARDUINO_STM32_FEATHER)
  typedef volatile unsigned int RwReg;
  //#define USE_FAST_PINIO
 #else
  typedef volatile uint32_t RwReg;
  #define USE_FAST_PINIO
 #endif
#elif defined (__AVR__) || defined(TEENSYDUINO)
  typedef volatile uint8_t RwReg;
  #define USE_FAST_PINIO
#elif defined (ESP8266) || defined (ESP32)
  typedef volatile uint32_t RwReg;
#elif defined (__ARDUINO_ARC__)
  typedef volatile uint32_t RwReg;
#endif


#define Yp ADC_CHANNEL_6
#define Xm ADC_CHANNEL_7
#define _rxplate 285

#define MINX 500
#define MAXX 3600
#define MINY 370
#define MAXY 3800
#define MINPRES 50

typedef enum {INPUT,OUTPUT} Mode;

typedef struct  {

  int16_t x, y, z;

}TSPoint;


int16_t map(int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max);
void pinMode(GPIO_TypeDef* port, uint16_t pin,uint32_t mode);
int adc_read(uint32_t channel);
//TSPoint(void);
//TSPoint(int16_t x, int16_t y, int16_t z);
//bool operator==(TSPoint);
//bool operator!=(TSPoint);

  //TouchScreen(uint8_t xp, uint8_t yp, uint8_t xm, uint8_t ym, uint16_t rx);

  bool isTouching(void);
  uint16_t pressure(void);
  int readTouchY();
  int readTouchX();
  TSPoint getPoint();
  TSPoint getScreenPoint(TSPoint point);
  //int16_t pressureThreshhold;

  //uint8_t _yp, _ym, _xm, _xp;
 // uint16_t _rxplate;

  //volatile RwReg *xp_port, *yp_port, *xm_port, *ym_port;
  //RwReg xp_pin, xm_pin, yp_pin, ym_pin;

#endif
