#ifndef PMS5003ST_H
#define PMS5003ST_H

//#include"stm32f1xx_hal.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
/** \def BUFFER_SIZE
 * \brief Rozmiar tablicy danych z PMS
 *
 */
#define BUFFER_SIZE 40

/**
 * Struct which define PMS sensor configuration:
 * PMS_SET, PMS_RESET pins and UART handler.
 */
typedef struct {
/**
 * \brief UART handler, it must be configure by user.
 * 
 * Baud rate: 9600 bps.
 * Parity bits: None.
 * Stop bits: 1bit.
 */ 
UART_HandleTypeDef* PMS_huart;

/**
 * Pin to send PMS_SET signal, low reset.
 */
uint16_t PMS_SET_pin;
/**
 * GPIO port associated with the `PMS_SET_pin.
 */
GPIO_TypeDef* PMS_SET_port;
/**
 * Pin to send PMS_RESET signal, low reset.
 */
uint16_t PMS_RESET_pin;
/**
 * GPIO port associated with the `PMS_RESET_pin`.
 */
GPIO_TypeDef* PMS_RESET_port;

} PMS5003ST_config;

/**
 * Struct which is defined as PMS sensor.
 */
typedef struct {
    /**
    * Buffer for data received from sensor.
    */
    uint8_t PMS_DATA[40];
    /**
     * Frame lenght should be equal to 40 bytes.
     */
    int frame_length;
    /**
     *  PM1.0 concentration in ÎĽg/m3.
     */ 
    int PM1_0;
    /**
     *  PM2.5 concentration in ÎĽg/m3.
     */     
    int PM2_5;
    /**
     *  PM10 concentration in ÎĽg/m3.
     */ 
    int PM10;
    /**
     * PM1.0 concentration in ÎĽg/m3ďĽ�under atmospheric environment).
     */ 
    int PM1_0env;
    /**
     * PM2.5 concentration in ÎĽg/m3ďĽ�under atmospheric environment).
     */     
    int PM2_5env;
    /**
     * PM10 concentration in ÎĽg/m3ďĽ�under atmospheric environment.
     */ 
    int PM10env;
    /**
     * Number of particles with diameter beyond 0.3 ÎĽm in 0.1 L of air.
     */ 
    int particles_0_3;
    /**
     * Number of particles with diameter beyond 0.5 ÎĽm in 0.1 L of air.
     */ 
    int particles_0_5;
    /**
     * Number of particles with diameter beyond 1.0 ÎĽm in 0.1 L of air.
     */
    int particles_1_0;
    /**
     * Number of particles with diameter beyond 2.5 ÎĽm in 0.1 L of air.
     */
    int particles_2_5;
    /**
     * Number of particles with diameter beyond 5.0 ÎĽm in 0.1 L of air.
     */
    int particles_5_0;
    /**
     * Number of particles with diameter beyond 10 ÎĽm in 0.1 L of air.
     */
    int particles_10;
    /**
     * Formaldehyde concentration in mg/m3.
     */
    float formaldehyde;
    /**
     * The intake air temperature.
     */
    float temperature;
    /**
     * The intake air humidity.
     */
    float humidity;
    /**
     * Configuration of the sensor
     */
    PMS5003ST_config configuration;

} PMS5003ST;

/**
 *  \brief Function to set PMS hardware configuration: set, reset, UART pins. 
 * 
 * \param config Handler to PMS configuration structure which should be already created.
 * \param huart Handler to UART structure which define uart pins, baudrate, parity and stop bits.
 * \param set_pin Pin to connect PMS_SET signal.
 * \param set_port Port associated witch set pin.
 * \param reset_pin Pin to connect PMS_RESET signal.
 * \param reset_port Port associated witch reset pin.
 * \return void
 */ 
void PMS_set_configuration(PMS5003ST_config* config, UART_HandleTypeDef* huart,
 uint16_t set_pin, GPIO_TypeDef* set_port, uint16_t reset_pin, GPIO_TypeDef* reset_port);
/**
 * Initializes the DHT22 handle using the provided configuration.
 * \parm config - a pointer to the initialization structure.
 * \param sensor - a pointer to the DHT22 handle you want to initialize.
 */
void PMS_initialize(PMS5003ST_config* config, PMS5003ST* sensor);
/**
 * Function to activate uart read.
 * \param sensor Handler to PMS structure which should be already created.
 */ 
void PMS_read(PMS5003ST* sensor);

void PMS_sleepON(PMS5003ST* sensor);

void PMS_sleepOFF(PMS5003ST* sensor);

void PMS_reset(PMS5003ST* sensor);

int PMS_get_PM1_0(PMS5003ST* sensor);

int PMS_get_PM2_5(PMS5003ST* sensor);

int PMS_get_PM10(PMS5003ST* sensor);

int PMS_get_PM1_0env(PMS5003ST* sensor);

int PMS_get_PM2_5env(PMS5003ST* sensor);

int PMS_get_PM10env(PMS5003ST* sensor);

int PMS_get_part_0_3(PMS5003ST* sensor);

int PMS_get_part_0_5(PMS5003ST* sensor);

int PMS_get_part_1_0(PMS5003ST* sensor);

int PMS_get_part_2_5(PMS5003ST* sensor);

int PMS_get_part_5_0(PMS5003ST* sensor);

int PMS_get_part_10(PMS5003ST* sensor);

float PMS_get_form(PMS5003ST* sensor);

float PMS_get_temp(PMS5003ST* sensor);

float PMS_get_hum(PMS5003ST* sensor);
/**
 * This function should be trigger in the interrupt handler.
 */ 
void PMS_get_data(PMS5003ST* sensor);

void PMS_set_active_mode(PMS5003ST* sensor);

void PMS_set_passive_mode(PMS5003ST* sensor);

#endif
