/*
 * sd_functions.h
 *
 *  Created on: 16.01.2019
 *      Author: Blazej
 */

#ifndef SD_FUNCTIONS_H_
#define SD_FUNCTIONS_H_
#include "stm32f4xx_hal.h"
#include "ff_gen_drv.h"
#include "fatfs.h"

FRESULT open_append (
		FIL* fp,            /* [OUT] File object to create */
		const char* path    /* [IN]  File name to be opened */
);
void writeHeader();
void writeData();


#endif /* SD_FUNCTIONS_H_ */
