#ifndef _ADAFRUIT_GFX_AS_H
#define _ADAFRUIT_GFX_AS_H

#include "Load_fonts.h"
#include "stdint.h"
//#include <Adafruit_GFX.h>

#define swap(a, b) { int16_t t = a; a = b; b = t; }

/** This class provides a few extensions to Adafruit_GFX, mostly for compatibility with
 *  existing code. Note that the fonts ("size" parameter) are not the same ones use as the
 *  ones provided by Adafruit_GFX. Using any of the functions defined in this class will
 *  therefore pull additional font tables into flash. If that is an issue, try to stick
 *  to the base class, or trim down the fonts loaded in Load_fonts.h . */

    int16_t drawUnicode(uint16_t uniCode, int16_t x, int16_t y, int16_t size, int16_t bgcolor);
    int16_t drawNumber(long long_num,int16_t poX, int16_t poY, int16_t size,int16_t bgcolor);
    int16_t drawChar(char c, int16_t x, int16_t y, int16_t size, int16_t bgcolor);
    int16_t drawString(char *string, int16_t poX, int16_t poY, int16_t size, int16_t bgcolor);
    int16_t drawCentreString(char *string, int16_t dX, int16_t poY, int16_t size,int16_t bgcolor);
    int16_t drawRightString(char *string, int16_t dX, int16_t poY, int16_t size,int16_t bgcolor);
    int16_t drawFloat(float floatNumber,int16_t decimal,int16_t poX, int16_t poY, int16_t size,int16_t bgcolor);

    void drawRoundRect(int16_t x, int16_t y, int16_t w,
            int16_t h, int16_t r, uint16_t color);
    void fillRoundRect(int16_t x, int16_t y, int16_t w,
            int16_t h, int16_t r, uint16_t color);
    void fillCircleHelper(int16_t x0, int16_t y0, int16_t r,
            uint8_t cornername, int16_t delta, uint16_t color);
    void drawCircleHelper( int16_t x0, int16_t y0,
            int16_t r, uint8_t cornername, uint16_t color);

    void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color);

    void writeLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1,uint16_t color);
    void drawCircle(int16_t x0, int16_t y0, int16_t r,uint16_t color);
    void fillCircle(int16_t x0, int16_t y0, int16_t r,uint16_t color);
#endif // _ADAFRUIT_GFX_AS_H
