/*
 * DHT22.h
 *
 *  Created on: 03.12.2018
 *      Author: Patryk Forencewicz
 */

#ifndef DHT22_H_
#define DHT22_H_

typedef struct {
	/**
	 * PIN to which the sensor was connected.
	 */
	uint16_t DHT22_pin;
	/**
	 * GPIO port associated with the DHT22_pin.
	 */
	GPIO_TypeDef* DHT22_port;
	/**
	 * The air temperature.
	 */
	float Temperature;
	/**
	 * The air humidity.
	 */
	float Humidity;
	/**
	 * The CRC checksum.
	 */
	uint8_t checksum;

} DHT22;

/**
 *  Function to set DHT22 1 wire pin.
 *
 * \param config Handler to DHT configuration structure which should be already created.
 * \param set_pin Pin to connect PMS_SET signal.
 * \param set_port Port associated witch set pin.
 * \return void
 */
void DHT_set_configuration(DHT22*, uint16_t, GPIO_TypeDef*);
/**
 *  Set 1 wire pin as output
 *
 * \param config Handler to DHT configuration structure which should be already created.
 * \return void
 */
void DHT_set_gpio_output(DHT22*);
/**
 *  Set 1 wire pin as input
 *
 * \param config Handler to DHT configuration structure which should be already created.
 * \return void
 */
void DHT_set_gpio_input(DHT22*);
/**
 *  The DHT initialization procedure
 *\param config Handler to DHT configuration structure which should be already created.
 * \return void
 */
void DHT_start(DHT22*);
/**
 *  Wait for sensor response.
 *\param config Handler to DHT configuration structure which should be already created.
 * \return void
 */
uint8_t DHT_check_response(DHT22*);
/**
 *  Read one byte from DHT sensor
 *\param config Handler to DHT configuration structure which should be already created.
 * \return One byte of data stored in DHT sensor
 */
uint8_t DHT_read_byte(DHT22*);
/**
 *  Read all data from DHT22 sensor
 *\param config Handler to DHT configuration structure which should be already created.
 * \return If CRC is correct return 1, else 0.
 */
int DHT_read_data(DHT22*);
/**
 *  Get temperature value which is read from sensor.
 *\param config Handler to DHT structure which should be already created.
 * \return Temperature value in Celcius degrees.
 */
float DHT_get_temperature(DHT22*);
/**
 *  Get humidity value which is read from sensor.
 *\param config Handler to DHT structure which should be already created.
 * \return Humidity value in %.
 */
float DHT_get_humidity(DHT22*);

#endif /* DHT22_H_ */
