/*
 * sensors_I2C.h
 *
 *  Created on: 16.01.2019
 *      Author: Blazej
 */

#ifndef SENSORS_I2C_H_
#define SENSORS_I2C_H_

#include "stm32f4xx_hal.h"

uint8_t readBatteryValue();
int readCO2();
void readTVOC(int *tvoc, int *co2eq);

#endif /* SENSORS_I2C_H_ */
