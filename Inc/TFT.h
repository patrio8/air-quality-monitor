/** \file TFT.h*/
#include "Adafruit_HX8357.h"
#include "Adafruit_GFX_AS.h"
#include "TouchScreen.h"

/**
 * \defgroup WYMIARY Wymiary ekranu
 *
 * \{
 */
#define WIDTH 480 /**<Szerokosc ekranu w pikselach*/
#define HIGHT 320 /**<Wyskosc ekranu w pikselach*/
/** \} */

/**
 * \defgroup KOLORY Dostepne kolory
 *
 * \{
 */
#define BLACK 0x0000
#define BLUE 0x001F
#define RED 0xF800
#define GREEN 45 << 5
#define MAGENTA 0xF81F
#define YELLOW 0xFFE0
#define WHITE 0xFFFF
#define GREY 0xA514
/** \} */
/**
 * \brief Maksymalna liczba pol na ekranie glownym
 */
#define MAIN_SCR_MAX_AREAS 9
/**
 * \brief Liczba pomiarow
 */
#define MAX_NUM_OF_MESUREMENTS 15
/**
 * \brief Maksymalna liczba pol na ekranie szczegolowym
 */
#define CIRC_SCR_MAX_AREAS 3
/**
 * \brief Szerokosc jednego znaku dla czcionki 7-segmentowej
 */
#define SEV_SEGMENT_WIDTH 16

/**
 * \defgroup PKT_GLOWNE Definicje punktow poczatkowych pol ekranu glownego
 *
 *Pola ekranu glownego sa prostokatami, ich poczatkowe punkty znajduja sie w lewym gornym rogu.
 * \{
 */
#define P1x 0
#define P1y 32
#define P2x 160
#define P2y 32
#define P3x 320
#define P3y 32
#define P4x 0
#define P4y 128
#define P5x 160
#define P5y 128
#define P6x 320
#define P6y 128
#define P7x 0
#define P7y 224
#define P8x 160
#define P8y 224
#define P9x 320
#define P9y 224
/** \} */

/**
 * \defgroup WYMIARY_POLA_EKRANU_GLOWNEGO Wymiary pola ekranu glownego
 *
 * \{
 */
#define MAIN_SCR_AREA_WIDTH 154 /**<Szerokosc pola ekranu glownego w pikselach */
#define MAIN_SCR_AREA_HIGTH 92 /**< Wysokosc pola ekranu glownego w pikselach */
/** \} */


/**
 * \defgroup PKT_SZCZEGOLOWE Definicje punktow srodkowych pol ekranu szczegolowego.
 *
 *Pola ekranu szczegolowego sa okregami, wiec do ich wyrysowania potrzebny jest punkt srodkowy i promien.
 *Jesli na ekranie maja byc trzy pola nalezy uzyc punktow Pcirc3_***, jesli maja byc dwa pola nalezy uzyc Pcirc2_***.
 * \{
 */
#define Pcirc3_x1 240
#define Pcirc3_y1 120

#define Pcirc3_x2 95
#define Pcirc3_y2 220

#define Pcirc3_x3 385
#define Pcirc3_y3 220

#define Pcirc2_x1 360
#define Pcirc2_y1 200

#define Pcirc2_x2 120
#define Pcirc2_y2 200
/** \} */

#define DETAIL_CIRC_RADIUS 82 /**<Promien okregu jesli na ekranie maja byc wyswietlane 3 pola*/
/**
 * \defgroup DETAIL_BCK_BTN Definicja przycisku "Cofnij" na ekranie szczegolowym
 *
 * \{
 */
#define DETAIL_BCK_BTN_X 6 /**< Wspolrzedna x - lewy gory rog*/
#define DETAIL_BCK_BTN_Y 39 /**< Wspolrzedna y - lewy gory rog*/
#define DETAIL_BCK_BTN_W 100 /**< Szerokosc*/
#define DETAIL_BCK_BTN_H 50 /**< Wysokosc*/
/** \} */

/**
 * \brief Typ definiujacy rodzaje mozliwych pomiarow
 */
typedef enum
{
	PM1_0,/**<Pomiar pylu zawieszonego PM1.0*/
	PM2_5,/**<Pomiar pylu zawieszonego PM2.5*/
	PM10, /**<Pomiar pylu zawieszonego PM10*/
	CO2, /**<Pomiar dwutlenku wegla*/
	HCHO, /**<Pomiar formaldehydu*/
	TEMP, /**<Pomiar temeratury*/
	HUM, /**<Pomiar wilgotnosci*/
	VOC,/**<Pomiar lotnych zwiazkow organicznych*/
	LPG,/**<Pomiar gazu LPG*/
	PART_03,/**<Pomiar czastek o rozmiarach mniejszych od 0.3 um*/
	PART_05,/**<Pomiar czastek o rozmiarach mniejszych od 0.5 um*/
	PART_1, /**<Pomiar czastek o rozmiarach mniejszych od 1 um*/
	PART_2_5, /**<Pomiar czastek o rozmiarach mniejszych od 2.5 um*/
	PART_5, /**<Pomiar czastek o rozmiarach mniejszych od 5 um*/
	PART_10 /**<Pomiar czastek o rozmiarach mniejszych od 0.3 um*/
}
MeasureType;

/**
 * \brief Typ definiujacy aktualnie wyswietlany ekran
 */
typedef enum
{
	MainScr,/**<Pierwszy ekran glowny. Zawiera najwazniejsze pomiary.*/
	SecondScr,/**<Drugi ekran glowny. Zawiera pozostale pomiary*/
	LineChartScr,/**<Ekran z danymi archiwalnymi przedstawionymi w formie wykresu liniowego*/
	BarChartScr,/**<Ekran z danymi archiwalnymi przedstawionymi w formie wykresu slupkowego*/
	PM1_0DetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru PM1.0*/
	PM2_5DetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru PM2.5*/
	PM10DetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru PM10*/
	TempDetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru temperatury*/
	HumDetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru wilgotnosci*/
	CO2DetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru dwutlenku wegla*/
	HCHODetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru formaldehydu*/
	VOCDetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru lotnych zwiazkow organicznych*/
	LPGDetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru gazu LPG*/
	PART_03DetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru czastek o rozmiarach mniejszych od 0.3 um*/
	PART_05DetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru czastek o rozmiarach mniejszych od 0.5 um*/
	PART_1DetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru czastek o rozmiarach mniejszych od 1 um*/
	PART_2_5DetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru czastek o rozmiarach mniejszych od 2.5 um*/
	PART_5DetailScr,/**<Ekran szczegolowy z danymi dotyczacymi pomiaru czastek o rozmiarach mniejszych od 5 um*/
	PART_10DetailScr, /**<Ekran szczegolowy z danymi dotyczacymi pomiaru czastek o rozmiarach mniejszych od 10 um*/
	SettingsScr /**<Ekran z ustawieniami czasu i daty. */
}
CurrentScreen;
/**
 * \brief Typ definiujacy rodzaj wyswietlanego wykresu
 */
typedef enum {
	Bar,/**< Wykres slupkowy*/
	Line/**< Wykres liniowy*/
}
ChartType;

#define TOPSCR_TITLE_MAX_CHAR_NUM 35 /**< Maksymalny rozmiar tablicy przechowujacej tytul ekranu*/
#define TIME_DATE_MAX_CHAR_NUM 4 /**< Maksymalny rozmiar tablic przechowujacych godzine, sekunde, minuty, dni oraz miesiace */
#define YEAR_DATE_MAX_CHAR_NUM 5 /**< Maksymalny rozmiar tablicy przechowujacej rok*/
/**
 * \brief Struktura opisujaca obszar gorny ekranu. Zawiera nazwe aktualnie wyswietlanego ekranu, godzine oraz date.
 */
typedef struct
{

	char Title[TOPSCR_TITLE_MAX_CHAR_NUM];/**< Tablica przechowujaca nazwe ekranu*/
	int Sx;/**< Punkt poczatkowy tekstu z nazwa ekranu, wspolrzedna x - lewy gorny rog ekranu*/
	int Sy;/**< Punkt poczatkowy tekstu z nazwa ekranu, wspolrzedna y - lewy gorny rog ekranu*/
	int Ssize; /**<Rozmiar tekstu z nazwa ekranu */
	int Scolor;/**<Kolor tla tekstu */


	char hour [TIME_DATE_MAX_CHAR_NUM]; /**< Tablica przechowujaca aktualna godzine*/
	char min[TIME_DATE_MAX_CHAR_NUM]; /**< Tablica przechowujaca aktualna minute*/
	char sec[TIME_DATE_MAX_CHAR_NUM]; /**< Tablica przechowujaca aktualna sekunde*/

	int Tx;/**< Punkt poczatkowy wyswietlania czasu - wpolrzedna x*/
	int Ty;/**< Punkt poczatkowy wyswietlania czasu - wpolrzedna y*/
	int Tsize; ;/**<Rozmiar tekstu z godzina*/
	int Tcolor;/**<Kolor tla tekstu z godzina */


	char year[YEAR_DATE_MAX_CHAR_NUM]; /**< Tablica przechowujaca rok*/
	char month[TIME_DATE_MAX_CHAR_NUM]; /**< Tablica przechowujaca miesiac*/
	char day[TIME_DATE_MAX_CHAR_NUM]; /**< Tablica przechowujaca dzien*/

	int Dx;/**< Punkt poczatkowy wyswietlania czasu - wpolrzedna x*/
	int Dy;/**< Punkt poczatkowy wyswietlania czasu - wpolrzedna y*/
	int Dsize;/**<Rozmiar tekstu z data*/
	int Dcolor;/**<Kolor tla tekstu z data */


}
TitleArea;

#define AREA_UNIT_MAX_CHAR_NUM 15 /**<Maksymalny rozmiar tablicy przechowujacej nazwe jednostki*/
#define AREA_NAME_MAX_CHAR_NUM 20 /**<Maksymalny rozmiar tablicy przechowujacej nazwe pomiaru*/

/**
 * \brief Struktura opisujaca pojedynczy kafelek wyswietlany na ekranie glownym.
 * Wyswietlane na nim sa: nazwa pomiaru, aktualna wartosc oraz jednostka.
 */
typedef struct {

	int x0; /**< Punkt poczatkowy obszaru - wspolrzedna x */
	int y0; /**< Punkt poczatkowy obszaru - wspolrzedna y */

	int w; /**< Szerokosc obszaru*/
	int h; /**< Wysokosc obszaru*/
	int r; /**< Promien - prostokaty sa zaokraglone*/


	int color; /**< Kolor tla obszaru: domyslnie - zielony,
		po przekroczeniu progu informowania - zolty,
		po przekroczeniu progu alarmowego - czerwony
	 */


	int x1; /**< Punkt poczatkowy obszaru wyswietlana nazwy pomiaru - wspolrzedna x */
	int y1; /**< Punkt poczatkowy obszaru wyswietlana nazwy pomiaru - wspolrzedna y */

	char areaName[AREA_UNIT_MAX_CHAR_NUM]; /**<Tablica przechowujaca nazwe pomiaru*/

	//Value start point
	int x2; /**< Punkt poczatkowy obszaru wyswietlana wartosci pomiaru - wspolrzedna x */
	int y2; /**< Punkt poczatkowy obszaru wyswietlana wartosci pomiaru - wspolrzedna y */

	int val_width; /**< Szerokosc obszaru wyswietlania wartosci - potrzebne do odswiezania*/
	int val_hight; /**< Wysokosc obszaru wyswietlania wartosci - potrzebne do odswiezania*/

	int if_float; /**<Zmienna definiujaca czy wartosc jest rzeczywista:
	0 - wartosc calkowita; 1 - wartosc rzeczywista */
	int decimal; /**< Jesli wartosc jest rzeczywsta to parametr ten mowi ile miejsc po przeciku powinno byc wyswietlane */
	float value; /**<Aktualna wartosc pomiaru do wyswietlania */

	int x3; /**< Punkt poczatkowy obszaru wyswietlana jednostki - wspolrzedna x */
	int y3; /**< Punkt poczatkowy obszaru wyswietlana jednostki - wspolrzedna y */

	char unit[AREA_UNIT_MAX_CHAR_NUM]; /**<Tablica przechowujaca jednostke pomiaru*/

	//Value of first and second Threshold
	int yellow; /**<Wartosc progu informowania */
	int red; /**< Wartosc progu alarmowania*/


}
MainScrArea;

//#define AREA_UNIT_MAX_CHAR_NUM 10

#define CHART_Y_VALUES_MAX_NUM_1 6 /**< Jesli na wykresie maja byc wyswietlane pomiary z ostatnich 6 godzin*/
#define CHART_Y_VALUES_MAX_NUM_2 12 /**< Jesli na wykresie maja byc wyswietlane pomiary z ostatnich 12 godzin*/
#define CHART_Y_VALUES_MAX_NUM_3 24 /**< Jesli wykresie maja byc wyswietlane pomiary z ostatnich 24 godzin*/

#define CHART_Y_SPAN 21 /**< Szerokosc podzialki na osi Y wykresu*/
#define CHART_X_SPAN_1 68 /**< Wartosc co jaka wyswietlane sa wartosci na osi X - jesli wyswietlane sa wartosci z ostatnich 6 godzin */
#define CHART_X_SPAN 34 /**< Wartosc co jaka wyswietlane sa wartosci na osi X - jesli wyswietlane sa wartosci z ostatnich 12 lub 24 godzin*/

#define CHART_X_SPAN_VAL_1 68 /**< Szerokosc podzialki na osi X wykresu - jesli wyswetlane sa wartosci z ostatnich 6 godzin*/
#define CHART_X_SPAN_VAL_2 34 /**< Szerokosc podzialki na osi X wykresu - jesli wyswetlane sa wartosci z ostatnich 12 godzin*/
#define CHART_X_SPAN_VAL_3 17 /**< Szerokosc podzialki na osi X wykresu - jesli wyswetlane sa wartosci z ostatnich 24 godzin*/

#define CHART_X_Y_AXIS_X0 35 /**< Punkt poczatkowy osi X oraz Y - wspolrzedna x */
#define CHART_Y_AXIS_Y0 55 /**< Punkt poczatkowy osi Y - wspolrzedna y */
#define CHART_Y_AXIS_LEN 210 /**< Dlugosc osi Y*/
#define CHART_Y_LABLES_X0 3 /**< Punkt poczatkowy wyswietlania etykiet przy osi Y - wspolrzedna x */
#define CHART_Y_LABLES_Y0 257 /**< Punkt poczatkowy wyswietlania etykiet przy osi Y - wspolrzedna y */
#define CHART_Y_LABLES_NUM 10 /**< Liczba etykiet przy osi Y*/
#define CHART_X_AXIS_Y0 265 /**< Punkt poczatkowy osi X - wspolrzedna y */
#define CHART_X_AXIS_LEN 425 /**< Dlugosc osi X*/
#define CHART_X_LABLES_NUM 12 /**< Liczba etykiet przy osi X*/
#define CHART_X_LABLES_NUM_2 6
#define LINE_CHART_X_LABLES_X0 30 /**< Punkt poczatkowy wyswietlania etykiet przy osi X dla wykresu liniowego - wspolrzedna x */
#define BAR_CHART_X_LABELS_X0 45 /**< Punkt poczatkowy wyswietlania etykiet przy osi X dla wykresu slupkowego - wspolrzedna x */


/**
*\brief Struktura opisujaca wlasciwosci wykresu.
 */
typedef struct {

	char y_unit[AREA_UNIT_MAX_CHAR_NUM]; /**<Tablica przechowujaca jednostke osi Y*/
	int min_y_value; /**< Minimalna wartosc do wyswietlenia na wykresie*/
	int max_y_value; /**< Maksymalna wartosc do wyswietlenia na wykresie*/
	int y_values[CHART_Y_VALUES_MAX_NUM_3]; /**<Tablica przechowujaca wartosci do wyswietlenia na wykresie*/
	int y_span; /**< Szerokosc podzialki na osi Y wykresu*/


	char x_unit[AREA_UNIT_MAX_CHAR_NUM]; /**<Tablica przechowujaca jednostke osi Y*/
	int x_span; /**< Szerokosc podzialki na osi X wykresu*/

	int current_hour; /**< Aktualna godzina - od niej wyliczane sa etykiety na osi X.*/

	MeasureType measure; /**< Rodzaj pomiaru z jakim zwiazany jest wykres */

	char title[50]; /**< Tytul wykresu - umieszczany na gornym obszarze ekranu*/
}
Chart;
/**
*\brief Struktura opisujaca owalny obszar umieszczany na ekranie szczegolowym - moga byc maksymalnie 3.
* Wyswietlane na nim sa: nazwa pomiaru, aktualna wartosc oraz jednostka.
 */
typedef struct {

	//Start point
	int x0;
	int y0;

	//Radius
	int r;

	//Background color (default is WHITE)
	int color;

	//Title Area start point
	int x1;
	int y1;
	//Name
	char areaName[AREA_NAME_MAX_CHAR_NUM] ;

	//Value start point
	int x2;
	int y2;

	//Value area
	int val_width;
	int val_hight;

	//Value

	int if_float;
	int decimal; //if value is float decimal defines number of digits after dot.
	float value;

	//Unit area start point
	int x3;
	int y3;

	//Unit
	char unit[AREA_UNIT_MAX_CHAR_NUM];

	//Value of first and second Threshold
	int yellow;
	int red;
}
CircleArea;
//Function definitions
CurrentScreen MainScrTouchDetect(CurrentScreen current,CircleArea DetailScreen[][3], MainScrArea SecondScreen[]);
CurrentScreen SecondScrTouchDetect(CurrentScreen current,CircleArea DetailScreen[][3], MainScrArea MainScreen[]);
CurrentScreen DetailScrTouchDetect(CurrentScreen current, Chart* chart,MainScrArea MainScreen[],int last24val[][24],int current_h);
CurrentScreen ChartScrTouchDetect(Chart* chart, CurrentScreen current,CircleArea DetailScreen[][3]);
CurrentScreen SettingsScrTouchDetect(MainScrArea SecondScreen[],RTC_TimeTypeDef* time_to_set,RTC_DateTypeDef* date_to_set);


void InitTitleScrArea(TitleArea* area, char areaName[]);
void InitMainScrArea(MainScrArea* area, int x0, int y0, char areaName[], char unit[],int color,int yellow_alert, int red_alert,int if_float,int decimal);
void InitCircleArea(CircleArea* area, int x0, int y0,int r, char areaName[], char unit[],int color,int yellow_alert, int red_alert,int if_float, int decimal);
void InitMainScreen(MainScrArea MainScreen[]);
void InitSecondScreen(MainScrArea SecondScreen[]);
void InitDetailedScreen(CircleArea DetScr[][3]);
CurrentScreen DisplayMainScreen(MainScrArea MainScreen[]);
CurrentScreen DisplaySecondScreen(MainScrArea SecondScreen[]);
CurrentScreen DisplayDetailScreen(CircleArea DetailScreen[][3],char Title[],int index,int numofareas);

#define HOUR_SETT_X0 50
#define MIN_SETT_X0 125
#define TIME_SETT_Y0 100

#define DATE_SETT_Y0 100
#define DAY_SETT_X0 200
#define MONTH_SETT_X0 275
#define YEAR_SETT_X0 350

#define TIME_SET_BTN_X0 125
#define TIME_SET_BTN_YO 260
#define TIME_SET_BTN_W 200
#define TIME_SET_BTN_H 50

CurrentScreen DisplaySettingsScreen(RTC_TimeTypeDef* time_to_set, RTC_DateTypeDef* date_to_set);
void DrawTitleArea(TitleArea* area);
void DrawMainScrArea(MainScrArea* area);
void DrawCircleArea(CircleArea* area);
int Max(int tab[],int size);
int charsInTable(char tab[]);
void ClearCharTable(char table[],int size);
void CopyCharTable(char from[], char to[],int destsize);

void ChangeMainScrAreaColor(MainScrArea* area, int color);
int SetMainScrAreaValue(MainScrArea* area, float value);
void DrawMainScrAreaValue(MainScrArea* area);
int SetCircleScrAreaValue(CircleArea* area, float value);
void DrawCircleAreaValue(CircleArea* area);
void SetMainScrAreaThresholds(MainScrArea* area, int yellow, int red);
void SetMainScrAreaUnit(MainScrArea* area, char unit[]);
CurrentScreen RefreshMainScreen(CurrentScreen current,MainScrArea MainScreen[], volatile float AllValues[][3], MainScrArea MainScreen2[],CircleArea DetailScreen[][3]);
void RefreshDetailScreen(MeasureType measure_type,CircleArea area[][3],volatile float values[][3]);

#define CHART_BUTTON_Y 283
#define CHART_BUTTON_HIGHT 36
#define CHART_BUTTON_WIDTH 100
#define CHART_BUTTON1_X0 2
#define CHART_BUTTON2_X0 84
#define CHART_BUTTON3_X0 186

#define CHART_BUTTON_HOUR6_X0 305
#define CHART_BUTTON_HOUR12_X0 362
#define CHART_BUTTON_HOUR24_X0 422
#define CHART_BUTTON_HOUR_WIDTH 50

typedef struct{
//Start point
int x0;
int y0;
//Dimensions
int width;
int hight;

uint16_t color;
char name[15];
}
ChartButton;
void InitButton(ChartButton* button, int x0, int y0,int w, int h,uint16_t color, char name[]);
void DisplayChartButton(ChartButton* button);
void DisplayLineChart(Chart* chart, int numofhours);
void DisplayBarChart(Chart* chart, int numofhours);
int RecalculateValuetoChartPoint(int max, int val);

CurrentScreen ChartBackButtonPress(Chart* chart, CircleArea[][3]);
CurrentScreen DisplayChartArea(Chart* chart, int chart_type, int numofhours);



